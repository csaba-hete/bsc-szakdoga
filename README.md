# Project name :)

## Built With

* [Angular](https://angular.io/)
* [PHP](http://php.net/)
* [Laravel](https://laravel.com/)
* [Composer](https://getcomposer.org/)
* [Docker](https://www.docker.com/)

## Authors

* **Hete Csaba** - [gitlab](https://gitlab.com/csaba-hete), [github](https://github.com/csabahete)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details