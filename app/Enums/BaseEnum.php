<?php

namespace App\Enums;

use App\Exceptions\ConstantNotFoundException;
use ReflectionClass;

abstract class BaseEnum implements Enum
{
    private static $cache = [];

    private $name;
    private $value;

    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    public static function byName(string $name): Enum
    {
        $enum = self::getConstants()[$name] ?? null;
        if (!$enum) {
            $enumClassName = get_called_class();
            throw new ConstantNotFoundException("Enum '$name' not found in $enumClassName" );
        }
        return $enum;
    }

    public static function __callStatic($name, $args): Enum
    {
        return self::byName($name);
    }

    public function getConstName(): string
    {
        return $this->name;
    }

    public function getConstValue()
    {
        return $this->value;
    }

    private static function getConstants(): array
    {
        $enumClassName = get_called_class();
        if (!array_key_exists($enumClassName, self::$cache)) {
            self::$cache[$enumClassName] = [];
            $reflectionClass = new ReflectionClass($enumClassName);
            foreach ($reflectionClass->getConstants() as $const => $value) {
                self::$cache[$enumClassName][$const] = self::buildEnum($const, $value);
            }
        }

        return self::$cache[$enumClassName];
    }

    private static function buildEnum(string $name, $value)
    {
        return new static($name, $value);
    }
}