<?php

namespace App\Enums;

final class CacheKeyEnum extends BaseEnum
{
    const EMAIL_CHANGE_DATA = 'email_change_data';
}