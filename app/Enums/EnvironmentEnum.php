<?php

namespace App\Enums;

final class EnvironmentEnum extends BaseEnum
{
    const DEVELOPMENT = 'development';
    const PRODUCTION = 'production';
    const TESTING = 'testing';
}