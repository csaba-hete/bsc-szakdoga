<?php

namespace App\Enums;

final class LanguageEnum extends BaseEnum
{
    const hu = 'hu';
    const en = 'en';
}