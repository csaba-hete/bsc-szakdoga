<?php

namespace App\Enums;

final class TestTypeEnum extends BaseEnum
{
    const PUBLIC = 'public';
    const PRIVATE = 'private';
}