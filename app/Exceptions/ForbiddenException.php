<?php

namespace App\Exceptions;

class ForbiddenException extends RestException
{
    public function __construct($message = "The user have no permission to perform this operation")
    {
        parent::__construct(
            "You have no permission for this operation",
            "unauthorized",
            403,
            $message
        );
    }
}
