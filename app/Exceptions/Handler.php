<?php

namespace App\Exceptions;

use App;
use App\Enums\EnvironmentEnum;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\JsonResponse
     * @throws ConstantNotFoundException
     */
    public function render($request, Exception $exception)
    {
//        $productionEnv = EnvironmentEnum::PRODUCTION();
//        $actualEnvironment = EnvironmentEnum::byName(strtoupper(App::environment()));
//        if ($productionEnv === $actualEnvironment) {
//            return Response::json([
//                "success" => false,
//                "details" => "Oops, something went wrong...",
//            ],
//                400
//            );
//        }

        if ($exception instanceof RestException) {
            return Response::json([
                "id" => $exception->getId(),
                "error" => $exception->getMessage(),
                "details" => $exception->getDetail(),
                "status" => $exception->getStatus()
            ],
                $exception->getStatus());
        }

        if($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
        {
            return redirect()->route('home');
        }

        if ($exception instanceof HttpException) {
            return Response::json([
                "error" => $exception->getMessage(),
                "trace" => $exception->getTrace(),
                "type" => get_class($exception),
                "status" => $exception->getStatusCode()

            ],
                $exception->getStatusCode());
        }

        return parent::render($request, $exception);
    }
}
