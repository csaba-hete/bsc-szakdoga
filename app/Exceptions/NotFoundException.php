<?php

namespace App\Exceptions;

class NotFoundException extends RestException
{
    public function __construct($message = '')
    {
        parent::__construct(
            "Not found!",
            "not_found",
            404,
            $message
        );
    }
}
