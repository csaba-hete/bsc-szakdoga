<?php

namespace App\Exceptions;

class NotPresentableCollectionException extends RestException
{
}
