<?php

namespace App\Exceptions;

use Exception;

class RestException extends Exception
{
    /**
     * @var @string
     * the identifier id ot the exception
     */
    protected $id;

    /**
     * @var @string
     * HTTP response status
     */
    protected $status;

    /**
     * @var @string
     * The title of the exception
     */
    protected $title;

    /**
     * @var @string
     * The details of the exception
     */
    protected $detail;

    /**
     * @param string $title
     * @param string $status
     * @param string $id
     * @param string $detail
     */
    public function __construct(string $title, ?string $id = "general_error", ?string $status = "500", ?string $detail = "")
    {
        parent::__construct($title);
        $this->title = $title;
        $this->id = $id;
        $this->status = $status;
        $this->detail = $detail;
    }

    /**
     * Get the status
     *
     * @return int
     */
    public function getStatus()
    {
        return (int)$this->status;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function __toString()
    {
        $exceptionString =  "Exception: ".get_class($this)." with data:";
        $exceptionString .= "[title: ".$this->title.", id:". $this->id;
        $exceptionString .= ", detail: ".$this->detail.", status: ".$this->status."]";
        $exceptionString .= "\nStack trace:\n".$this->getTraceAsString();
        return $exceptionString;
    }
}
