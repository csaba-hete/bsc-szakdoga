<?php

namespace App\Exceptions;

class TranslationNotFoundException extends RestException
{
    public function __construct($message = '')
    {
        parent::__construct(
            "Translation not found!",
            "no_translation",
            404,
            $message
        );
    }
}
