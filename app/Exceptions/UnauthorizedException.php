<?php

namespace App\Exceptions;

class UnauthorizedException extends RestException
{
    public function __construct($message = "The user is not authenticated")
    {
        parent::__construct(
            "Unauthorized operation",
            "unauthorized",
            401,
            $message
        );
    }
}
