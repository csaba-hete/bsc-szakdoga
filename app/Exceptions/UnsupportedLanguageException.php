<?php

namespace App\Exceptions;

class UnsupportedLanguageException extends RestException
{
    public function __construct($message = '')
    {
        parent::__construct(
            "Unsupported language",
            "unsupported_language",
            400,
            $message
        );
    }
}
