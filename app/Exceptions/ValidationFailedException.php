<?php

namespace App\Exceptions;

class ValidationFailedException extends RestException
{
    public function __construct($message = '')
    {
        parent::__construct(
            "Request validation failed",
            "validation_failed",
            412,
            $message
        );
    }
}
