<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Services\AuthService;
use App\Services\UserService;
use Auth;

class AuthController extends RestController
{
    /** @var UserService */
    private $userService;

    /** @var AuthService */
    private $authService;

    public function __construct(UserService $userService, AuthService $authService)
    {
        $this->userService = $userService;
        $this->authService = $authService;
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $token = $this->authService->login($credentials);
        return $this->buildResponse([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => config('jwt.ttl') * 60,
        ]);
    }

    public function logout()
    {
        if (Auth::guard('api')->check()) {
            $this->authService->logout();
        }
        return $this->buildResponse("Logout successful!");
    }
}
