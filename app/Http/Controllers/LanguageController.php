<?php

namespace App\Http\Controllers;

use App\Services\LanguageService;

class LanguageController extends RestController
{
    /** @var LanguageService */
    private $languageService;

    public function __construct(LanguageService $languageService)
    {
        $this->languageService = $languageService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $this->buildResponse($this->languageService->getLanguages());
    }
}
