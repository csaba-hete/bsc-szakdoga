<?php

namespace App\Http\Controllers;

use App\Enums\LanguageEnum;
use App\Exceptions\ConstantNotFoundException;
use App\Exceptions\UnsupportedLanguageException;
use App\Http\Requests\GetPhrasesRequest;
use App\Services\PhraseService;
use App\Services\UserService;
use Illuminate\Http\Request;

class PhraseController extends RestController
{
    /** @var UserService */
    private $userService;

    /** @var PhraseService */
    private $phraseService;

    /**
     * PhraseController constructor.
     * @param UserService $userService
     * @param PhraseService $phraseService
     */
    public function __construct(UserService $userService, PhraseService $phraseService)
    {
        $this->userService = $userService;
        $this->phraseService = $phraseService;
    }


    public function history(?int $count = null)
    {
        $user = $this->userService->getUser();
        $phrases = $this->phraseService->getSearchHistory($user, $count);

        return $this->buildResponse($phrases);
    }

    public function getUserPhrases(GetPhrasesRequest $request)
    {
        try {
            $language = LanguageEnum::byName($request->get('language'));
        } catch (ConstantNotFoundException $e) {
            throw new UnsupportedLanguageException();
        }

        $user = $this->userService->getUser();
        $phrases = $this->phraseService->getPhrases($user, $language);

        return $this->buildResponse($phrases);
    }

    public function getRandomPhrases(?int $count = null)
    {
        $phrases = $this->phraseService->getRandomPhrases($count);

        return $this->buildResponse($phrases);
    }
}
