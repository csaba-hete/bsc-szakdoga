<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;
use Response;

abstract class RestController extends Controller
{
    protected function buildResponse($result, ?string $message = '')
    {
        if ($result instanceof Collection) {
            $result = $result->values();
        }
        return Response::json([
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ]);
    }
}
