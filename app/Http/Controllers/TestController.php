<?php

namespace App\Http\Controllers;

use App\Enums\LanguageEnum;
use App\Enums\TestTypeEnum;
use App\Exceptions\ConstantNotFoundException;
use App\Exceptions\NotFoundException;
use App\Exceptions\UnsupportedLanguageException;
use App\Http\Requests\CreateTestRequest;
use App\Http\Requests\EvaluateTestRequest;
use App\Http\Requests\GenerateTestRequest;
use App\Models\Test;
use App\Presenters\TestListPresenter;
use App\Presenters\TestPresenter;
use App\Services\TestService;
use App\Services\UserService;
use App\User;
use Request;

class TestController extends RestController
{
    /** @var TestService */
    private $testService;

    /** @var UserService */
    private $userService;

    /**
     * TestController constructor.
     * @param TestService $testService
     * @param UserService $userService
     */
    public function __construct(TestService $testService, UserService $userService)
    {
        $this->testService = $testService;
        $this->userService = $userService;
    }


    public function index(Request $request)
    {
        $user = $this->userService->getUser();
        $publicTests = $this->testService->getPublicTests();
        $privateTests = $this->testService->getPrivateTests($user);
        $tests = $publicTests->merge($privateTests)->sortByDesc('id');
        return $this->buildResponse((new TestListPresenter($user))->collection($tests));
    }

    public function show(int $id)
    {
        $user = $this->userService->getUser();
        $test = $this->getTest($id, $user);
        return $this->buildResponse((new TestPresenter())->present($test));
    }

    public function generate(GenerateTestRequest $request)
    {
        $user = $this->userService->getUser();
        try {
            $srcLang = LanguageEnum::byName($request['src_lang']);
            $dstLang = LanguageEnum::byName($request['dst_lang']);
        } catch (ConstantNotFoundException $e) {
            throw new UnsupportedLanguageException();
        }

        $test = $this->testService->generate($user, $srcLang, $dstLang);

        return $this->buildResponse((new TestPresenter())->present($test));
    }

    public function store(CreateTestRequest $request)
    {
        try {
            $fromLanguage = LanguageEnum::byName($request['src_lang']);
            $toLanguage = LanguageEnum::byName($request['dst_lang']);
        } catch (ConstantNotFoundException $e) {
            throw new UnsupportedLanguageException();
        }

        $user = $this->userService->getUser();
        $phrases = $user->phrases()->whereIn('phrases.id', $request['phrases'])->get();
        $type = TestTypeEnum::byName($request['type']);
        $test = $this->testService->createTestFromPhrases($phrases, $fromLanguage, $toLanguage, $type, $request['name']);
        $user->testsOwned()->save($test);
        return $this->buildResponse($test);
    }

    public function evaluate(int $id, EvaluateTestRequest $request)
    {
        $user = $this->userService->getUser();
        $test = $this->getTest($id, $user);
        $answers = $request->get('quizzes');
        $results = $this->testService->evaluateTest($test, $answers, $user);

        return $this->buildResponse($results);
    }

    public function destroy(int $id)
    {
        $user = $this->userService->getUser();
        $test = $user->testsOwned()->find($id);
        if (!$test) {
            throw new NotFoundException("Test not found!");
        }
        $test->delete();
        return $this->buildResponse([]);
    }

    private function getTest(int $id, ?User $user = null): Test
    {
        $privateType = TestTypeEnum::PRIVATE()->getConstName();
        $test = $this->testService->getTest($id);
        if (empty($test) || ($test->type === $privateType && (!$user || $test->owner_id !== $user->id))) {
            throw new NotFoundException();
        }
        return $test;
    }
}
