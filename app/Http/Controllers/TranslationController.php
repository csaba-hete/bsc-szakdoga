<?php

namespace App\Http\Controllers;

use App\Enums\LanguageEnum;
use App\Exceptions\ConstantNotFoundException;
use App\Exceptions\UnsupportedLanguageException;
use App\Http\Requests\TranslationRequest;
use App\Services\TranslationService;

class TranslationController extends RestController
{
    /** @var TranslationService */
    private $translationService;

    /**
     * TranslationController constructor.
     * @param TranslationService $translationService
     */
    public function __construct(TranslationService $translationService)
    {
        $this->translationService = $translationService;
    }


    public function translate(TranslationRequest $request)
    {
        try {
            $srcLang = LanguageEnum::byName($request['src_lang']);
            $dstLang = LanguageEnum::byName($request['dst_lang']);
        } catch (ConstantNotFoundException $e) {
            throw new UnsupportedLanguageException();
        }

        $data = $this->translationService->translate($srcLang, $dstLang, $request['phrase']);
        return $this->buildResponse($data);
    }
}
