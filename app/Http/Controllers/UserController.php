<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Exceptions\UnauthorizedException;
use App\Http\Requests\ChangeEmailRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Presenters\UserProfileInfoPresenter;
use App\Services\AuthService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

class UserController extends RestController
{
    /** @var UserService */
    private $userService;

    /** @var AuthService */
    private $authService;

    public function __construct(UserService $userService, AuthService $authService)
    {
        $this->userService = $userService;
        $this->authService = $authService;
    }

    public function store(RegisterUserRequest $request)
    {
        $user = new User([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'username' => $request['username'],
            'password' => $request['password'],
        ]);

        $user = $this->userService->createUser($user);
        return $this->buildResponse($user);
    }

    public function show()
    {
        return $this->buildResponse($this->authService->getUser());
    }

    public function getProfileInfo()
    {
        $user = $this->userService->getUser();
        return $this->buildResponse((new UserProfileInfoPresenter())->present($user));
    }

    public function update(UpdateUserRequest $request)
    {
        $user = $this->userService->getUser();
        $firstName = $request->get('first_name');
        $lastName = $request->get('last_name');
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->save();
        return $this->buildResponse((new UserProfileInfoPresenter())->present($user));
    }

    public function destroy()
    {
        $user = $this->userService->getUser();
        $user->delete();
        return $this->buildResponse('');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $oldPassword = $request->get('old_password');
        $newPassword = $request->get('password');
        $this->userService->changePassword($oldPassword, $newPassword);
        return $this->buildResponse('');
    }

    public function changeEmail(ChangeEmailRequest $request)
    {
        $email = $request->get('email');
        $this->userService->prepareEmailChange($email);
        return $this->buildResponse('');
    }

    public function confirmEmailChange(Request $request)
    {
        $email = $request->get('email');
        $token = $request->get('token');
        try {
            $user = $this->userService->confirmEmailChange($email, $token);
            return view('email-change-confirmed', [
                'newEmail' => $user->email
            ]);
        } catch (UnauthorizedException|NotFoundException $e) {
            return view('email-change-error');
        }
    }
}
