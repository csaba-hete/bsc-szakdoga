<?php

namespace App\Http\Requests;

use App\User;

class ChangePasswordRequest extends RestRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => User::$rules['password'],
            'password' => User::$rules['password'],
        ];
    }
}
