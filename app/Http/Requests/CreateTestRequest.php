<?php

namespace App\Http\Requests;

class CreateTestRequest extends RestRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:3,255',
            'src_lang' => 'required|string|size:2|different:dst_lang',
            'dst_lang' => 'required|string|size:2|different:src_lang',
            'type' => 'required|in:PUBLIC,PRIVATE',
            'phrases' => 'required|array',
        ];
    }
}
