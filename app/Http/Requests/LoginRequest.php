<?php

namespace App\Http\Requests;

use App\User;

class LoginRequest extends RestRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $loginCredentialRules = array_filter(User::$rules, function ($key) {
            return in_array($key, ['email', 'password']);
        }, ARRAY_FILTER_USE_KEY);

        return array_map(function ($value) {
            return str_replace('|unique:users', '', $value);
        }, $loginCredentialRules);
    }
}
