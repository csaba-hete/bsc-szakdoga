<?php

namespace App\Http\Requests;

use App\Exceptions\ForbiddenException;
use App\Exceptions\ValidationFailedException;
use Illuminate\Contracts\Validation\Validator;

abstract class RestRequest extends APIRequest
{
    public function rules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    protected function failedAuthorization()
    {
        throw new ForbiddenException();
    }

    protected function failedValidation(Validator $validator)
    {
        $error = 'The following validations failed: ';
        $messages = array_flatten($validator->getMessageBag()->toArray());
        foreach ($messages as $key => $message) {
            $error .= ($key + 1) . '. ' . $message . ' ';
        }
        throw new ValidationFailedException($error);
    }
}
