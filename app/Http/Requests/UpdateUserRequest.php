<?php

namespace App\Http\Requests;

use App\User;

class UpdateUserRequest extends RestRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => User::$rules['first_name'],
            'last_name' => User::$rules['last_name'],
        ];
    }
}
