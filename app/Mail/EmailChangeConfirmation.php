<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailChangeConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User */
    public $user;
    /** @var string */
    public $url;
    /** @var string */
    public $email;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $url
     * @param string $newEmail
     */
    public function __construct(User $user, string $url, string $newEmail)
    {
        $this->user = $user;
        $this->url = $url;
        $this->email = $newEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@slt.com')
            ->subject('Email address change confirmation')
            ->view('emails.email-change-confirmation');
    }
}
