<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Phrase
 *
 * @property int $id
 * @property string $language_code
 * @property string $phrase
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Translation[] $bwdTranslations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Translation[] $fwdTranslations
 * @property-read \App\Models\Language $language
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PhraseUserPivot[] $searchedPhraseDescriptors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Phrase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Phrase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Phrase whereLanguageCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Phrase wherePhrase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Phrase whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Phrase extends Model
{
    public static $rules = [
        'phrase' => 'required|string|max:255',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language_code', 'phrase',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_code', 'language_code');
    }

    public function translations()
    {
        return $this->fwdTranslations()->union($this->bwdTranslations());
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function searchedPhraseDescriptors()
    {
        return $this->hasMany(PhraseUserPivot::class);
    }

    public function fwdTranslations()
    {
        return $this->hasMany(Translation::class, 'phrase1_id');
    }

    public function bwdTranslations()
    {
        return $this->hasMany(Translation::class, 'phrase2_id');
    }
}
