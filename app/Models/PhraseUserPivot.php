<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PhraseUserPivot
 *
 * @property int $id
 * @property int $phrase_id
 * @property int $user_id
 * @property string $dst_lang
 * @property int $untrained_index
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Phrase $phrase
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot whereDstLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot wherePhraseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot whereUntrainedIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PhraseUserPivot whereUserId($value)
 * @mixin \Eloquent
 */
class PhraseUserPivot extends Model
{
    protected $table = 'phrase_user';

    protected $fillable = [
        'phrase_id', 'user_id', 'untrained_index', 'dst_lang',
    ];

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_code', 'dst_lang');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function phrase()
    {
        return $this->belongsTo(Phrase::class);
    }

    public function setUntrainedIndexAttribute($untrainedIndex)
    {
        if ($untrainedIndex <= 0) {
            $untrainedIndex = 0;
        } else if ($untrainedIndex >= 10) {
            $untrainedIndex = 10;
        }
        $this->attributes ['untrained_index'] = $untrainedIndex;
    }
}
