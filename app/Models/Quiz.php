<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Quiz
 *
 * @property int $id
 * @property int $test_id
 * @property int $translatable_id
 * @property int $solution_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Phrase $solution
 * @property-read \App\Models\Test $test
 * @property-read \App\Models\Phrase $translatable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereSolutionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereTranslatableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Quiz whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Quiz extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'translatable_id', 'solution_id',
    ];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function translatable()
    {
        return $this->belongsTo(Phrase::class, 'translatable_id');
    }

    public function solution()
    {
        return $this->belongsTo(Phrase::class, 'solution_id');
    }
}
