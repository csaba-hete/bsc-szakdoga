<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Test
 *
 * @property int $id
 * @property string $name
 * @property string $from_language
 * @property string $to_language
 * @property int|null $owner_id
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Language $fromLanguage
 * @property-read \App\User|null $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Quiz[] $quizzes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TestUserPivot[] $takenTestDescriptors
 * @property-read \App\Models\Language $toLanguage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereFromLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereToLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Test whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Test extends Model
{
    public static $rules = [
        'from_language' => 'required|string|max:2',
        'to_language' => 'required|string|max:2',
        'name' => 'between:3,255',
        'type' => 'required|in:PUBLIC,PRIVATE',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_language', 'to_language', 'name', 'type'
    ];

    public function translations()
    {
        return $this->fwdTranslations()->union($this->bwdTranslations());
    }

    public function fromLanguage()
    {
        return $this->belongsTo(Language::class, 'from_language');
    }

    public function toLanguage()
    {
        return $this->belongsTo(Language::class, 'to_language');
    }

    public function languages()
    {
        return $this->fromLanguage()->union($this->toLanguage());
    }

    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }

    public function takenTestDescriptors()
    {
        return $this->hasMany(TestUserPivot::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withPivot('id', 'last_resolution_ratio', 'created_at', 'updated_at')
            ->as('users_tested')
            ->withTimestamps()
            ->orderByDesc('pivot_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id')->withDefault(null);
    }
}
