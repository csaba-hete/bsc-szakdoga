<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TestUserPivot
 *
 * @property int $id
 * @property int $user_id
 * @property int $test_id
 * @property float $last_resolution_ratio
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Test $test
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestUserPivot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestUserPivot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestUserPivot whereLastResolutionRatio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestUserPivot whereTestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestUserPivot whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TestUserPivot whereUserId($value)
 * @mixin \Eloquent
 */
class TestUserPivot extends Model
{
    protected $table = 'test_user';

    protected $fillable = [
        'last_resolution_ratio', 'user_id', 'test_id',
    ];

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setLastResolutionRatioAttribute($lastResolution)
    {
        if ($lastResolution <= 0) {
            $lastResolution = 0;
        } else if ($lastResolution >= 100) {
            $lastResolution = 100.0;
        }
        $this->attributes ['last_resolution_ratio'] = $lastResolution;
    }
}
