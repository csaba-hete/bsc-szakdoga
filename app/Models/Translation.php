<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Models\Translation
 *
 * @property int $id
 * @property int $phrase1_id
 * @property int $phrase2_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Phrase $phrase1
 * @property-read \App\Models\Phrase $phrase2
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translation wherePhrase1Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translation wherePhrase2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Translation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phrase1_id', 'phrase2_id'
    ];

    public function getPhrases(): Collection
    {
        return collect([
            $this->phrase1()->get(),
            $this->phrase2()->get(),
        ]);
    }

    public function phrase1()
    {
        return $this->belongsTo(Phrase::class, 'phrase1_id');
    }

    public function phrase2()
    {
        return $this->belongsTo(Phrase::class, 'phrase2_id');
    }
}
