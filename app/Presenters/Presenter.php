<?php

namespace App\Presenters;

use Illuminate\Database\Eloquent\Model;

interface Presenter
{
    public function present(Model $model);
}