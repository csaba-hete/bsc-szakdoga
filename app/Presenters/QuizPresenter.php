<?php

namespace App\Presenters;

use App\Models\Phrase;
use App\Models\Quiz;
use Illuminate\Database\Eloquent\Model;

class QuizPresenter implements Presenter
{
    const FAKE_CHOICES = 3;
    const PHRASE_ATTRIBUTES = [
        'id',
        'language_code',
        'phrase',
        'created_at',
        'updated_at',
    ];

    public function present(Model $model)
    {
        /** @var Quiz $model */
        $choices = [];
        $translationIds = $model->translatable->fwdTranslations->pluck('phrase2_id')
            ->merge(
                $model->translatable->bwdTranslations->pluck('phrase1_id')
            )->toArray();

        Phrase::whereNotIn('id', $translationIds)
            ->where('language_code', '=', $model->test->to_language)
            ->inRandomOrder()
            ->limit(self::FAKE_CHOICES)
            ->get()
            ->push($model->solution)
            ->each(function ($item) use (&$choices) {
                $choices[] = $item->only(self::PHRASE_ATTRIBUTES);
            });
        shuffle($choices);
        return [
            'id' => $model->id,
            'phrase' => $model->translatable->only(self::PHRASE_ATTRIBUTES),
            'choices' => $choices,
        ];
    }
}