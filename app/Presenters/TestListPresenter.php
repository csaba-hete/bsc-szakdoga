<?php

namespace App\Presenters;

use App\Models\Test;
use App\Models\TestUserPivot;
use App\Traits\CollectionPresenter;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TestListPresenter implements Presenter
{
    use CollectionPresenter;

    /** @var User */
    private $user;

    public function __construct(?User $user)
    {
        $this->user = $user;
    }

    public function present(Model $model)
    {
        /** @var Test $model */
        $presented = $model->toArray();
        $presented['created_by'] = $model->owner->username ?? 'system';
        if ($this->user) {
            /** @var TestUserPivot $takenTestDescriptor */
            $takenTestDescriptor = $this->user->takenTestDescriptors()
                ->where('test_id', '=', $model->id)
                ->first();
            if ($takenTestDescriptor){
                $presented['last_resolution_ratio'] = $takenTestDescriptor->last_resolution_ratio;
                $presented['last_taken_at'] = $takenTestDescriptor->updated_at->format("Y-m-d H:i:s");
            }
        }
        return $presented;
    }
}