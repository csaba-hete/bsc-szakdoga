<?php

namespace App\Presenters;

use App\Models\Test;
use Illuminate\Database\Eloquent\Model;

class TestPresenter implements Presenter
{
    public function present(Model $model)
    {
        /** @var Test $model */
        $quizzes = [];
        $quizPresenter = new QuizPresenter();
        foreach ($model->quizzes as $quiz) {
            $quizzes[] = $quizPresenter->present($quiz);
        }

        return [
            'id' => $model->id,
            'from_language' => $model->from_language,
            'to_language' => $model->to_language,
            'name' => $model->name,
            'quizzes' => $quizzes,
        ];
    }
}