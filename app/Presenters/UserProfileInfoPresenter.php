<?php

namespace App\Presenters;

use App\Models\Test;
use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProfileInfoPresenter implements Presenter
{
    public function present(Model $model)
    {
        /** @var User $model */
        $presented = $model->toArray();

        $presented['tests_taken'] = $model->testsTaken()->count();
        $presented['avg_test_result'] = $model->testsTaken()->avg('last_resolution_ratio');
        $presented['phrases_searched'] = $model->searchedPhraseDescriptors()->count();
        $presented['phrases_learned'] = $model->searchedPhraseDescriptors()->where('untrained_index', '=', 0)->count();
        $presented['avg_phrase_untrained_index'] = $model->searchedPhraseDescriptors()->avg('untrained_index');
        return $presented;
    }
}