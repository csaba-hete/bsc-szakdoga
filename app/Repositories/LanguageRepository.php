<?php

namespace App\Repositories;

use App\Models\Language;
use Illuminate\Support\Collection;

class LanguageRepository
{
    public function all(): Collection
    {
        return Language::all();
    }
}