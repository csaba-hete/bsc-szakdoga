<?php

namespace App\Repositories;

use App\Enums\LanguageEnum;
use App\Models\Phrase;

class PhraseRepository
{
    public function create(Phrase $phrase): Phrase
    {
        $phrase->save();
        return $phrase;
    }

    /**
     * @param LanguageEnum $lang
     * @param string $text
     * @return Phrase
     */
    public function findPhrase(LanguageEnum $lang, string $text): ?Phrase
    {
        $attributesToFindBy = [
            ['language_code', '=', $lang->getConstName()],
            ['phrase', '=', $text],
        ];
        return Phrase::where($attributesToFindBy)->first();
    }
}