<?php

namespace App\Repositories;

use App\Enums\TestTypeEnum;
use App\Exceptions\TranslationNotFoundException;
use App\Models\Phrase;
use App\Models\Quiz;
use App\Models\Test;
use App\Models\Translation;
use Illuminate\Support\Collection;

class TestRepository
{

    public function create(Test $test, Collection $phrases): Test
    {
        \DB::transaction(function() use ($test, $phrases) {
            $test->save();
            $this->generateQuizzesToTest($test, $phrases);
        });
        return $test;
    }

    private function generateQuizzesToTest(Test $test, Collection $phrases): void
    {
        foreach ($phrases as $phrase) {
            /** @var Phrase $phrase */
            $translatedPhrase = $phrase->fwdTranslations()
                ->whereHas('phrase2', function ($query) use ($test) {
                    $query->where('language_code', '=', $test->to_language);
                })
                ->get()
                ->map(function(Translation $translation) {
                    return $translation->phrase2;
                })
                ->merge($phrase->bwdTranslations()
                    ->whereHas('phrase1', function ($query) use ($test) {
                        $query->where('language_code', '=', $test->to_language);
                    })
                    ->get()
                    ->map(function(Translation $translation) {
                        return $translation->phrase1;
                    })
                )
                ->random();

            if (empty($translatedPhrase)) {
                throw new TranslationNotFoundException();
            }
            $quiz = new Quiz([
                'translatable_id' => $phrase->id,
                'solution_id' => $translatedPhrase->id,
            ]);
            $test->quizzes()->save($quiz);
        }
    }

    public function getTests(TestTypeEnum $type): Collection
    {
        return Test::where('type', '=', $type->getConstName())->get();
    }

    public function find(int $id): ?Test
    {
        return Test::find($id);
    }
}