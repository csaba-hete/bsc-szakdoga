<?php

namespace App\Repositories;

use App\Enums\LanguageEnum;
use App\Models\Phrase;
use App\Models\Translation;
use DB;
use Illuminate\Support\Collection;

class TranslationRepository
{
    public function create(Phrase $phrase1, Phrase $phrase2)
    {
        $translation = Translation::firstOrCreate([
            'phrase1_id' => $phrase1->id,
            'phrase2_id' => $phrase2->id,
        ]);
        return $translation;
    }

    public function translate(LanguageEnum $srcLang, LanguageEnum $dstLang, Phrase $phrase): Collection
    {
        $phrases = $this->getTranslations($srcLang, $dstLang, $phrase);
        return $this->collectPhrasesFromTranslations($dstLang, $phrases);
    }

    /**
     * @param LanguageEnum $srcLang
     * @param LanguageEnum $dstLang
     * @param Phrase $phrase
     * @return Collection
     */
    private function getTranslations(LanguageEnum $srcLang, LanguageEnum $dstLang, Phrase $phrase): Collection
    {
        $srcLangCode = $srcLang->getConstName();
        $dstLangCode = $dstLang->getConstName();
        return Translation::join('phrases as p1', 'translations.phrase1_id', '=', 'p1.id')
            ->join('phrases as p2', 'translations.phrase2_id', '=', 'p2.id')
            ->where([
                ['p1.phrase', '=', $phrase->phrase],
                ['p1.language_code', '=', $srcLangCode],
                ['p2.language_code', '=', $dstLangCode],
            ])
            ->orWhere([
                ['p2.phrase', '=', $phrase->phrase],
                ['p2.language_code', '=', $srcLangCode],
                ['p1.language_code', '=', $dstLangCode],
            ])
            ->get();
    }

    /**
     * @param LanguageEnum $dstLang
     * @param Collection $translations
     * @return Collection of Phrases or an empty Collection
     */
    private function collectPhrasesFromTranslations(LanguageEnum $dstLang, Collection $translations): Collection
    {
        $dstLangCode = $dstLang->getConstName();
        return $translations->map(function (Translation $t) {
            return $t->getPhrases();
        })->flatten()->filter(function (Phrase $phrase) use ($dstLangCode) {
            return $phrase->language_code === $dstLangCode;
        });
    }
}