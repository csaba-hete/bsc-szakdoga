<?php

namespace App\Repositories;

use App\User;

class UserRepository
{

    public function create(User $user)
    {
        $user->save();
        return $user;
    }
}