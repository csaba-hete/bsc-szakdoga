<?php

namespace App\Services;

use App\Exceptions\NotFoundException;
use App\Exceptions\UnauthorizedException;
use App\User;
use Auth;
use JWTAuth;
use Log;

class AuthService
{
    public function login(array $credentials): string
    {
        Log::debug("User attempting to log in with the following email address: " . $credentials['email']);
        $this->checkUserExistence($credentials);
        // verify the credentials and create a token for the user
        if (!$token = JWTAuth::attempt($credentials)) {
            throw new UnauthorizedException("Invalid credentials");
        }
        Log::debug("User logged in with the following email address: " . $credentials['email']);
        return $token;
    }

    public function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
    }

    public function getUser()
    {
        return Auth::guard()->user();
    }

    public function isAuthenticated()
    {
        return Auth::check();
    }

    private function checkUserExistence(array $credentials): void
    {
        $user = User::whereEmail($credentials['email'])->first();
        if (!$user) {
            throw new NotFoundException("No such user!");
        }
    }
}