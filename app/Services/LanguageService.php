<?php

namespace App\Services;

use App\Repositories\LanguageRepository;
use Illuminate\Support\Collection;

class LanguageService
{
    /** @var LanguageRepository */
    private $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function getLanguages(): Collection
    {
        return $this->languageRepository->all();
    }
}