<?php

namespace App\Services;

use App\Enums\LanguageEnum;
use App\Models\Phrase;
use App\Repositories\PhraseRepository;
use App\User;
use Illuminate\Support\Collection;

class PhraseService
{
    /** @var PhraseRepository */
    private $phraseRepository;

    /**
     * PhraseService constructor.
     * @param $phraseRepository
     */
    public function __construct(PhraseRepository $phraseRepository)
    {
        $this->phraseRepository = $phraseRepository;
    }

    public function findPhrase(LanguageEnum $language, string $text): ?Phrase
    {
        return $this->phraseRepository->findPhrase($language, $text);
    }

    public function findOrCreatePhrase(LanguageEnum $language, string $text) {
        $phrase = $this->findPhrase($language, $text);
        if (empty($phrase)) {
            $phrase = new Phrase();
            $phrase->language_code = $language->getConstName();
            $phrase->phrase = $text;
            $phrase = $this->phraseRepository->create($phrase);
        }
        return $phrase;
    }

    public function getRandomTranslatablePhrases(int $limit, LanguageEnum $srcLang, LanguageEnum $dstLang, array $idNotIn = [])
    {
        return Phrase::whereNotIn('id', $idNotIn)
            ->where('language_code', '=', $srcLang->getConstName())
            ->whereHas('fwdTranslations', function ($query) use ($dstLang) {
                $query->whereHas('phrase2', function ($query) use ($dstLang) {
                        $query->where('language_code', '=', $dstLang->getConstName());
                    });
            })
            ->orWhereHas('bwdTranslations', function ($query) use ($dstLang) {
                $query->whereHas('phrase1', function ($query) use ($dstLang) {
                        $query->where('language_code', '=', $dstLang->getConstName());
                    });
            })
            ->inRandomOrder(time())
            ->get()
            ->slice(0, $limit);
    }

    public function getSearchHistory(User $user, ?int $limit = null): Collection
    {
         $phrases = $user->searchedPhraseDescriptors()
            ->with('phrase')
            ->orderBy('id');
         if ($limit) {
             return $phrases->limit($limit)->get();
         }
        return $phrases->get();
    }

    public function getPhrases(User $user, LanguageEnum $language): Collection
    {
        return $user->phrases()->where('language_code', '=', $language->getConstName())->get();
    }

    public function getRandomPhrases(?int $count = null): Collection
    {
        $inRandomOrder = Phrase::inRandomOrder(time());

        if ($count) {
            return $inRandomOrder->limit($count)->get();
        }
        return $inRandomOrder->get();
    }
}