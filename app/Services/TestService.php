<?php

namespace App\Services;

use App\Enums\LanguageEnum;
use App\Enums\TestTypeEnum;
use App\Exceptions\NotFoundException;
use App\Exceptions\ValidationFailedException;
use App\Models\Phrase;
use App\Models\PhraseUserPivot;
use App\Models\Quiz;
use App\Models\Test;
use App\Repositories\TestRepository;
use App\User;
use Illuminate\Support\Collection;
use SplPriorityQueue;

class TestService
{
    const TEST_QUIZ_COUNT = 10;

    /** @var TestRepository */
    private $testRepository;

    /** @var PhraseService */
    private $phraseService;

    /**
     * TestService constructor.
     * @param TestRepository $testRepository
     * @param PhraseService $phraseService
     */
    public function __construct(TestRepository $testRepository, PhraseService $phraseService)
    {
        $this->testRepository = $testRepository;
        $this->phraseService = $phraseService;
    }


    public function generate(?User $user, LanguageEnum $srcLang, LanguageEnum $dstLang): Test
    {
        if ($user) {
            $phrases = $this->getTestPhrasesForUser($user, self::TEST_QUIZ_COUNT, $srcLang, $dstLang);
        } else {
            $phrases = $this->phraseService->getRandomTranslatablePhrases(self::TEST_QUIZ_COUNT, $srcLang, $dstLang);
        }
        if ($phrases->isEmpty()) {
            throw new ValidationFailedException("Cannot create test from empty phrase list!");
        }

        return $this->createTestFromPhrases($phrases, $srcLang, $dstLang, TestTypeEnum::PUBLIC());
    }

    private function getTestPhrasesForUser(
        User $user,
        int $count,
        LanguageEnum $srcLang,
        LanguageEnum $dstLang
    ): Collection {
        $searchedPhraseDescriptors = $user->searchedPhraseDescriptors()
            ->where('dst_lang', '=', $dstLang->getConstName())
            ->whereHas('phrase', function ($query) use ($srcLang) {
                $query->where('language_code', '=', $srcLang->getConstName());
            })
            ->with('phrase')
            ->get();

        $phrasesFromUser = $this->getUntrainedPhrases($searchedPhraseDescriptors, $count);
        $missingPhraseCount = self::TEST_QUIZ_COUNT - $phrasesFromUser->count();
        if ($missingPhraseCount) {
            $phraseIds = $phrasesFromUser->pluck('id')->toArray();
            $randomPhrases = $this->phraseService->getRandomTranslatablePhrases($missingPhraseCount, $srcLang, $dstLang,
                $phraseIds);
            $phrasesFromUser = $phrasesFromUser->merge($randomPhrases);
        }
        return $phrasesFromUser;
    }

    public function getUntrainedPhrases(Collection $phraseDescriptors, int $limit = self::TEST_QUIZ_COUNT): Collection
    {
        $priorityQueue = new SplPriorityQueue();

        $phraseDescriptors->each(function (PhraseUserPivot $descriptor) use ($priorityQueue) {
            $priority = random_int(0, ($descriptor->untrained_index ?: random_int(0, 1)) * 10000);
            $priorityQueue->insert($descriptor->phrase, $priority);
        });

        $phrases = [];
        for ($i = 0; $i < $limit && !$priorityQueue->isEmpty(); ++$i) {
            $priorityQueue->setExtractFlags(SplPriorityQueue::EXTR_PRIORITY);
            if ($priorityQueue->top()) {
                $priorityQueue->setExtractFlags(SplPriorityQueue::EXTR_DATA);
                $phrases[] = $priorityQueue->extract();
            }
        }

        return collect($phrases);
    }

    public function createTestFromPhrases(
        Collection $phrases,
        LanguageEnum $fromLanguage,
        LanguageEnum $toLanguage,
        TestTypeEnum $type,
        string $name = 'Generated test'
    ): Test {
        $test = new Test([
            "from_language" => $fromLanguage->getConstName(),
            "to_language" => $toLanguage->getConstName(),
            "type" => $type->getConstName(),
            "name" => $name,
        ]);
        return $this->testRepository->create($test, $phrases);
    }

    public function getPublicTests(): Collection
    {
        return $this->testRepository->getTests(TestTypeEnum::PUBLIC());
    }

    public function getPrivateTests(?User $user): Collection
    {
        if ($user) {
            return $user->testsOwned()->where('type', '=', TestTypeEnum::PRIVATE ()->getConstName())->get();
        }
        return collect([]);
    }

    public function getTest(int $id): ?Test
    {
        return $this->testRepository->find($id);
    }

    public function evaluateTest(Test $test, array $answers, ?User $user = null): array
    {
        $results = [
            'correct' => [],
            'incorrect' => [],
        ];
        foreach ($answers as $quizId => $answerId) {
            /** @var Quiz $quiz */
            $quiz = $test->quizzes()->find($quizId);
            if (empty($quiz)) {
                throw new NotFoundException("Quiz $quizId not found for test $test->id!");
            }
            $answerWasCorrect = $quiz->solution->id === $answerId;
            $this->recordQuizResult($quiz->translatable, $answerWasCorrect, $user);
            if ($answerWasCorrect) {
                $results['correct'][] = [
                    'phrase' => $quiz->translatable->phrase,
                    'answer' => $quiz->solution->phrase,
                ];
            } else {
                $results['incorrect'][] = [
                    'phrase' => $quiz->translatable->phrase,
                    'answer' => Phrase::find($answerId)->phrase,
                ];
            }
        }
        $this->recordTestResult($test, count($results['correct']), $user);
        return $results;
    }

    private function recordQuizResult(Phrase $phrase, bool $answerWasCorrect, ?User $user): void
    {
        if (empty($user)) {
            return;
        }

        $phraseDescriptor = $user->searchedPhraseDescriptors()
            ->whereHas('phrase', function ($query) use ($phrase) {
                $query->where('id', '=', $phrase->id);
            })->first();
        if ($phraseDescriptor) {
            $untrainedIndex = $phraseDescriptor->untrained_index;
            $phraseDescriptor->untrained_index = $answerWasCorrect ? --$untrainedIndex : ++$untrainedIndex;
            $phraseDescriptor->save();
        }
    }

    private function recordTestResult(Test $test, int $correctAnswerCount, ?User $user): void
    {
        if (empty($user)) {
            return;
        }

        $user->testsTaken()->syncWithoutDetaching([
            $test->id => [
                'last_resolution_ratio' => $correctAnswerCount / $test->quizzes->count() * 100.0,
            ]
        ]);
    }
}