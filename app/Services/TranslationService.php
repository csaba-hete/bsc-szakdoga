<?php

namespace App\Services;

use App\Enums\LanguageEnum;
use App\Models\Phrase;
use App\Repositories\TranslationRepository;
use App\Utils\Translators\ExternalTranslator;
use Illuminate\Support\Collection;

class TranslationService
{
    /** @var AuthService */
    private $authService;

    /** @var UserService */
    private $userService;

    /** @var PhraseService */
    private $phraseService;

    /** @var TranslationRepository */
    private $translationRepository;

    /**
     * TranslationService constructor.
     * @param AuthService $authService
     * @param UserService $userService
     * @param PhraseService $phraseService
     * @param TranslationRepository $translationRepository
     */
    public function __construct(
        AuthService $authService,
        UserService $userService,
        PhraseService $phraseService,
        TranslationRepository $translationRepository
    ) {
        $this->authService = $authService;
        $this->userService = $userService;
        $this->phraseService = $phraseService;
        $this->translationRepository = $translationRepository;
    }


    public function translate(LanguageEnum $srcLang, LanguageEnum $dstLang, string $text): Collection
    {
        return $this->getTranslations($srcLang, $dstLang, $text);
    }

    private function getTranslations(LanguageEnum $srcLang, LanguageEnum $dstLang, string $text): Collection
    {
        $phraseToTranslate = $this->phraseService->findPhrase($srcLang, $text);
        if (empty($phraseToTranslate)) {
            $phraseToTranslate = new Phrase();
            $phraseToTranslate->language_code = $srcLang->getConstName();
            $phraseToTranslate->phrase = $text;
        }

        $phrases = $this->translationRepository->translate($srcLang, $dstLang, $phraseToTranslate);
        if ($phrases->isEmpty()) {
            $phrases = $this->getTranslationsFromOuterSources($srcLang, $dstLang, $phraseToTranslate);
        }
        if ($phrases->isNotEmpty() && $this->authService->isAuthenticated()) {
            $phraseToTranslate = $this->phraseService->findOrCreatePhrase($srcLang, $phraseToTranslate->phrase);
            $this->userService->savePhraseToUser($this->userService->getUser(), $phraseToTranslate, $dstLang);
        }
        return $phrases;
    }

    private function getTranslationsFromOuterSources(LanguageEnum $srcLang, LanguageEnum $dstLang, Phrase $phraseToTranslate): Collection
    {
        $translatedPhrases = (new ExternalTranslator())->getTranslations($srcLang, $dstLang, $phraseToTranslate->phrase);
        if (!empty($translatedPhrases)) {
            $phraseToTranslate = $this->phraseService->findOrCreatePhrase($srcLang, $phraseToTranslate->phrase);
        }
        $translatedPhrases = array_map(function ($phrase) use ($dstLang, $phraseToTranslate) {
            $phrase = $this->phraseService->findOrCreatePhrase($dstLang, $phrase['phrase']);
            $this->translationRepository->create($phraseToTranslate, $phrase);
            return $phrase;
        }, $translatedPhrases);
        return collect($translatedPhrases);
    }
}