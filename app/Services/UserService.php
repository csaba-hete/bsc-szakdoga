<?php

namespace App\Services;

use App;
use App\Enums\CacheKeyEnum;
use App\Enums\LanguageEnum;
use App\Exceptions\ValidationFailedException;
use App\Mail\EmailChangeConfirmation;
use App\Models\Phrase;
use App\Repositories\UserRepository;
use App\User;
use Auth;
use Cache;

class UserService
{
    const EMAIL_CHANGE_DATA_TTL = 30;
    /** @var UserRepository */
    private $userRepository;

    /** @var User */
    private $user = null;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUser(): ?User
    {
        if (!$this->user && Auth::check()) {
            $this->user = Auth::user();
        }
        return $this->user;
    }

    public function createUser(User $user): User
    {
        $this->userRepository->create($user);
        return $user;
    }

    public function savePhraseToUser(User $user, Phrase $phrase, LanguageEnum $dstLang): void
    {
        $user->phrases()->syncWithoutDetaching([
            $phrase->id => [
                'dst_lang' => $dstLang->getConstName(),
            ],
        ]);
    }

    public function changePassword(string $oldPassword, string $newPassword): void
    {
        $user = $this->getUser();
        $auth = Auth::attempt(['email' => $user['email'], 'password' => $oldPassword]);
        if ($auth) {
            $user->password = $newPassword;
            $user->save();
        } else {
            throw new ValidationFailedException();
        }
    }

    public function prepareEmailChange(string $email): void
    {
        $user = $this->getUser();
        $expiresAt = now()->addMinutes(self::EMAIL_CHANGE_DATA_TTL);
        $token = bin2hex(random_bytes(16));
        $data = [
            'token' => $token,
            'email' => $email,
        ];
        $url = App::make('url')->to("email-change-confirmation?email={$user->email}&token=$token");
        Cache::put(CacheKeyEnum::EMAIL_CHANGE_DATA()->getConstName() . $user->email, serialize($data), $expiresAt);
        \Mail::to($user)->send(new EmailChangeConfirmation($user, $url, $email));
    }

    public function confirmEmailChange(string $email, string $token): User
    {
        /** @var User $user */
        $user = User::whereEmail($email)->first();
        $data = Cache::get(CacheKeyEnum::EMAIL_CHANGE_DATA()->getConstName() . $email);
        $data = unserialize($data);
        if (!$data || $data['token'] !== $token) {
            throw new App\Exceptions\UnauthorizedException();
        }
        if (!$user) {
            throw new App\Exceptions\NotFoundException();
        }
        Cache::offsetUnset(CacheKeyEnum::EMAIL_CHANGE_DATA()->getConstName() . $email);
        $user->email = $data['email'];
        $user->save();
        return $user;
    }
}