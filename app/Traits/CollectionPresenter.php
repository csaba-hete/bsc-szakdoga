<?php

namespace App\Traits;

use App\Exceptions\NotPresentableCollectionException;
use Illuminate\Support\Collection;

trait CollectionPresenter
{
    /**
     *  Warning: It ruins the original collection, replaces each entity with their representative array.
     *
     * @param Collection $collection
     * @return array
     * @throws NotPresentableCollectionException
     */
    public function collection($collection)
    {
        if (!($collection instanceof Collection)) {
            throw new NotPresentableCollectionException();
        }
        foreach ($collection as $key => $value) {
            $collection->put($key, $this->present($value));
        }

        return $collection->values()->toArray();
    }
}
