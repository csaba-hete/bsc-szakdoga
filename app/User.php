<?php

namespace App;

use App\Models\Phrase;
use App\Models\PhraseUserPivot;
use App\Models\Test;
use App\Models\TestUserPivot;
use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\User
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $full_name
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Test[] $testsOwned
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Test[] $testsTaken
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Phrase[] $phrases
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PhraseUserPivot[] $searchedPhraseDescriptors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TestUserPivot[] $takenTestDescriptors
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    public $appends = [
        'full_name',
    ];

    public static $rules = [
        'first_name' => 'required|string|between:3,50',
        'last_name' => 'required|string|between:3,50',
        'email' => 'required|email|unique:users',
        'username' => 'required|string|unique:users|between:3,50',
        'password' => 'required|string|min:5',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function phrases()
    {
        return $this->belongsToMany(Phrase::class)
            ->withPivot('id', 'dst_lang', 'untrained_index')
            ->as('search_history')
            ->withTimestamps()
            ->orderByDesc('pivot_id');
    }

    public function testsOwned()
    {
        return $this->hasMany(Test::class, 'owner_id');
    }

    public function testsTaken()
    {
        return $this->belongsToMany(Test::class)
            ->withPivot('id', 'last_resolution_ratio', 'created_at', 'updated_at')
            ->as('tests_taken')
            ->withTimestamps()
            ->orderByDesc('pivot_id');
    }

    public function searchedPhraseDescriptors()
    {
        return $this->hasMany(PhraseUserPivot::class);
    }

    public function takenTestDescriptors()
    {
        return $this->hasMany(TestUserPivot::class);
    }
}
