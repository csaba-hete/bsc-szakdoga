<?php

namespace App\Utils\Translators\Drivers;

use App\Enums\LanguageEnum;
use App\Exceptions\TranslationNotFoundException;
use Psr\Http\Message\ResponseInterface;

class GlosbeDriver implements TranslatorDriver
{
    private $url = 'https://glosbe.com/gapi/translate';

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getHttpMethod(): string
    {
        return TranslatorDriver::GET;
    }

    public function getRequestOptions(LanguageEnum $srcLang, LanguageEnum $dstLang, string $phrase): array
    {
        return [
            'query' => [
                'from' => $srcLang->getConstName(),
                'dest' => $dstLang->getConstName(),
                'phrase' => $phrase,
                'format' => 'json',
            ],
        ];
    }

    /** @inheritdoc */
    public function parseResponse(ResponseInterface $response): array
    {
        $responseBody = $this->validateAndDecodeResponse($response);
        return $this->parsePhrases($responseBody);
    }

    private function validateAndDecodeResponse(ResponseInterface $response)
    {
        if ($response->getStatusCode() !== 200) {
            throw new TranslationNotFoundException("No translation found at 'glosbe.com', response status:{$response->getStatusCode()}!");
        }
        $body = $this->decodeResponseBody($response);
        if (!array_key_exists('tuc', $body)) {
            throw new TranslationNotFoundException("No translation found at 'glosbe.com', offset for phrases changed!");
        };
        if (empty($body['tuc'])) {
            throw new TranslationNotFoundException("No translation found at 'glosbe.com'!");
        }
        return $body;
    }

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws TranslationNotFoundException
     */
    private function decodeResponseBody(ResponseInterface $response): array
    {
        $body = json_decode($response->getBody(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new TranslationNotFoundException("Error while decoding response from 'glosbe.com': " . json_last_error_msg());
        }
        return $body;
    }

    private function parsePhrases(array $responseBody): array
    {
        $phrases = [];
        foreach ($responseBody['tuc'] as $translation) {
            if (array_key_exists('phrase', $translation)) {
                $phrases[] = $translation['phrase']['text'];
            }
        }
        return $phrases;
    }
}