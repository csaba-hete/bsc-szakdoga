<?php

namespace App\Utils\Translators\Drivers;

use App\Enums\LanguageEnum;
use App\Exceptions\TranslationNotFoundException;
use Psr\Http\Message\ResponseInterface;

interface TranslatorDriver
{
    const GET = 'GET';
    const POST = 'POST';

    public function getHttpMethod(): string;

    public function getUrl(): string;

    public function getRequestOptions(LanguageEnum $srcLang, LanguageEnum $dstLang, string $phrase): array;

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws TranslationNotFoundException
     */
    public function parseResponse(ResponseInterface $response): array;
}