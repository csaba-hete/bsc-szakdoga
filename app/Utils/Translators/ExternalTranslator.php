<?php

namespace App\Utils\Translators;

use App;
use App\Enums\EnvironmentEnum;
use App\Enums\LanguageEnum;
use App\Exceptions\TranslationNotFoundException;
use App\Utils\Translators\Drivers\GlosbeDriver;
use App\Utils\Translators\Drivers\TranslatorDriver;
use GuzzleHttp\Client;
use Log;

class ExternalTranslator
{
    const SPECIAL_TEST_PHRASE = '****special****phrase****';
    private $translatorDrivers;

    /**
     * ExternalTranslator constructor.
     */
    public function __construct()
    {
        $this->translatorDrivers = collect([
            new GlosbeDriver(),
        ]);
    }


    /**
     * @param LanguageEnum $srcLang
     * @param LanguageEnum $dstLang
     * @param string $phrase
     * @return array of strings translated phrases (translated phrases)
     * @throws App\Exceptions\ConstantNotFoundException
     */
    public function getTranslations(LanguageEnum $srcLang, LanguageEnum $dstLang, string $phrase): array
    {
        $testingEnv = EnvironmentEnum::TESTING();
        $actualEnvironment = EnvironmentEnum::byName(strtoupper(App::environment()));
        $translations = [];

        if ($testingEnv === $actualEnvironment) {
            if ($phrase === self::SPECIAL_TEST_PHRASE) {
                $translations[] = self::SPECIAL_TEST_PHRASE;
            }
        } else {
            $this->translatorDrivers->each(function (TranslatorDriver $driver) use (
                &$translations,
                $srcLang,
                $dstLang,
                $phrase
            ) {
                $guzzleClient = new Client();
                $response = $guzzleClient->request(
                    $driver->getHttpMethod(),
                    $driver->getUrl(),
                    $driver->getRequestOptions($srcLang, $dstLang, $phrase)
                );
                try {
                    $translations = $driver->parseResponse($response);
                    if (!empty($translations)) {
                        return false; // this stops the iteration through all drivers
                    }
                } catch (TranslationNotFoundException $e) {
                    Log::warning("Translation not found: driver: " . get_class($driver) . ", details: " . $e->getDetail());
                }
            });
            $translations = $this->filterOutDuplicates($translations);
        }

        return array_map(function (string $text) {
            return ['phrase' => $text];
        }, $translations);
    }

    private function filterOutDuplicates(array $translations): array
    {
        return array_intersect_key(
            $translations,
            array_unique(array_map("strtolower", $translations))
        );
    }
}