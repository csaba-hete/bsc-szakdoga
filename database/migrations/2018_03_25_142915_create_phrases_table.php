<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhrasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('language_code', 2);
            $table->foreign('language_code')
                ->references('language_code')
                ->on('languages')
                ->onDelete('cascade');
            $table->string('phrase');
            $table->timestamps();

            $table->index(['language_code', 'phrase']);
            $table->unique(['language_code', 'phrase']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phrases', function (Blueprint $table) {
            $table->dropForeign('phrases_language_code_foreign');
        });

        Schema::dropIfExists('phrases');
    }
}
