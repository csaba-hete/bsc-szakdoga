<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phrase1_id')->unsigned();
            $table->foreign('phrase1_id')
                ->references('id')
                ->on('phrases')
                ->onDelete('cascade');
            $table->integer('phrase2_id')->unsigned();
            $table->foreign('phrase2_id')
                ->references('id')
                ->on('phrases')
                ->onDelete('cascade');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE `translations` ADD UNIQUE INDEX `forward_translations` (`phrase1_id`, `phrase2_id`)');
        DB::statement('ALTER TABLE `translations` ADD UNIQUE INDEX `backward_translations` (`phrase2_id`, `phrase1_id`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translations', function (Blueprint $table) {
            $table->dropForeign('translations_phrase1_id_foreign');
            $table->dropForeign('translations_phrase2_id_foreign');
        });
        Schema::dropIfExists('translations');
    }
}
