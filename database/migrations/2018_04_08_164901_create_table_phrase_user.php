<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePhraseUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phrase_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phrase_id')->unsigned();
            $table->foreign('phrase_id')
                ->references('id')
                ->on('phrases')
                ->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('dst_lang', 2);
            $table->foreign('dst_lang')
                ->references('language_code')
                ->on('languages')
                ->onDelete('cascade');
            $table->integer('untrained_index')->unsigned()->default(5);
            $table->timestamps();
        });

        DB::statement(
            'ALTER TABLE phrase_user 
                 ADD CONSTRAINT chk_untrained_index 
                 CHECK (0 <= untrained_index AND untrained_index <= 10);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('phrase_user', function (Blueprint $table) {
            $table->dropForeign('phrase_user_phrase_id_foreign');
            $table->dropForeign('phrase_user_user_id_foreign');
            $table->dropForeign('phrase_user_dst_lang_foreign');
        });

        Schema::dropIfExists('phrase_user');
    }
}
