<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('from_language', 2);
            $table->foreign('from_language')
                ->references('language_code')
                ->on('languages')
                ->onDelete('cascade');
            $table->string('to_language', 2);
            $table->foreign('to_language')
                ->references('language_code')
                ->on('languages')
                ->onDelete('cascade');
            $table->integer('owner_id')->unsigned()->nullable();
            $table->foreign('owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('type')->default('PUBLIC');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tests', function (Blueprint $table) {
            $table->dropForeign('tests_from_language_foreign');
            $table->dropForeign('tests_to_language_foreign');
            $table->dropForeign('tests_owner_id_foreign');
        });
        Schema::dropIfExists('tests');
    }
}
