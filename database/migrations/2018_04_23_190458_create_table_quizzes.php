<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuizzes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('test_id')->unsigned();
            $table->foreign('test_id')
                ->references('id')
                ->on('tests')
                ->onDelete('cascade');
            $table->integer('translatable_id')->unsigned();
            $table->foreign('translatable_id')
                ->references('id')
                ->on('phrases')
                ->onDelete('cascade');
            $table->integer('solution_id')->unsigned();
            $table->foreign('solution_id')
                ->references('id')
                ->on('phrases')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quizzes', function (Blueprint $table) {
            $table->dropForeign('quizzes_test_id_foreign');
            $table->dropForeign('quizzes_translatable_id_foreign');
            $table->dropForeign('quizzes_solution_id_foreign');
        });
        Schema::dropIfExists('quizzes');
    }
}
