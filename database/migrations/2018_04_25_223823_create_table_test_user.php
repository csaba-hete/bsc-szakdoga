<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTestUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('test_id')->unsigned();
            $table->foreign('test_id')
                ->references('id')
                ->on('tests')
                ->onDelete('cascade');
            $table->float('last_resolution_ratio')->unsigned()->default(0);
            $table->timestamps();
        });

        DB::statement(
            'ALTER TABLE test_user 
                 ADD CONSTRAINT chk_last_resolution_ratio 
                 CHECK (0 <= last_resolution_ratio AND last_resolution_ratio <= 100.0);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('test_user', function (Blueprint $table) {
            $table->dropForeign('test_user_user_id_foreign');
            $table->dropForeign('test_user_test_id_foreign');
        });
        Schema::dropIfExists('test_user');
    }
}
