<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
        \App\Models\Language::updateOrCreate(['name' => 'Hungarian', 'language_code' => 'hu']);
        \App\Models\Language::updateOrCreate(['name' => 'English', 'language_code' => 'en']);
    }
}
