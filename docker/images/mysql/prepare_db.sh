#!/usr/bin/env bash

OLDDIR=$PWD
HERE=$(dirname `which $0`)
cd $HERE

devuser='devuser'
devpass='devpassword'
testuser='testuser'
testpassword='testpassword'

mysqladmin -uroot -pexample create slt
mysqladmin -uroot -pexample create testing

mysql -uroot -pexample mysql <<__SQL
create user '$devuser'@'%' identified by '$devpass';
create user '$testuser'@'%' identified by '$testpassword';
grant all on slt.* to '$devuser'@'%';
grant all on testing.* to '$devuser'@'%';
grant all on testing.* to '$testuser'@'%';
__SQL