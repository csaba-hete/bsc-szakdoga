#!/usr/bin/env bash

docker-compose exec php php /var/www/html/slt/artisan config:clear
docker-compose exec php php /var/www/html/slt/artisan config:cache
while true; do
    echo "Starting migration..."
    echo "Migrating app DB..."
    php /var/www/html/slt/artisan migrate --seed
    APP_DB_MIGRATED=$?
    echo "Migrating test DB..."
    php /var/www/html/slt/artisan migrate --seed --database=testing
    TEST_DB_MIGRATED=$?
    if [ $APP_DB_MIGRATED -eq 0 ] && [ $TEST_DB_MIGRATED -eq 0 ]; then
        break
    else
        echo "Mysql wasn't ready, sleeping..."
    fi
    sleep 5
done

echo "Migration's finished!"