<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email change confirmed</title>
</head>
<body>
<h4>You have successfully changed your email address!</h4>
<p>
    Good news: you have successfully changed your email address to <strong>{{$newEmail}}</strong>!
    We are hoping that you enjoy using our application.
</p>
<p>
    The support team.
</p>
</body>
</html>