<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email change error</title>
</head>
<body>
<h4>There was a problem while trying to change your email address!</h4>
<p>
    There was a problem with the data you provided for us. Please try to submit it again.
</p>
<p>
    The support team.
</p>
</body>
</html>