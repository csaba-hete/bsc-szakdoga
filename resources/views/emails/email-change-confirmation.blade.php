<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email change confirmation</title>
</head>
<body>
    <h4>Hello {{$user->first_name}} {{$user->last_name}}!</h4>
    <p>
        This email was sent to you because you requested to change your email address to <strong>{{$email}}</strong>
        in our SearchLearnTest application!
        To confirm this change please open <a href="{{$url}}">this link</a>.

        The support team.
    </p>
</body>
</html>