<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function (\Illuminate\Routing\Router $router) {
    Route::post('login', '\App\Http\Controllers\AuthController@login');
    Route::post('logout', '\App\Http\Controllers\AuthController@logout');
});

Route::group(array(
    'middleware' => ['api', 'jwt.auth'],
), function () {
    Route::get('user', '\App\Http\Controllers\UserController@show');
    Route::put('user', '\App\Http\Controllers\UserController@update');
    Route::post('user/change-password', '\App\Http\Controllers\UserController@changePassword');
    Route::post('user/change-email', '\App\Http\Controllers\UserController@changeEmail');
    Route::delete('user', '\App\Http\Controllers\UserController@destroy');
    Route::get('user/phrases/history/{count?}', '\App\Http\Controllers\PhraseController@history');
    Route::get('user/phrases', '\App\Http\Controllers\PhraseController@getUserPhrases');
    Route::get('user/details', '\App\Http\Controllers\UserController@getProfileInfo');
    Route::resource('tests', '\App\Http\Controllers\TestController', ['only' => ['store', 'destroy']]);
});

Route::resource('tests', '\App\Http\Controllers\TestController', ['only' => ['index', 'show']]);
Route::post('tests/{id}/evaluate', '\App\Http\Controllers\TestController@evaluate');
Route::post('tests/generate', '\App\Http\Controllers\TestController@generate');
Route::post('user', '\App\Http\Controllers\UserController@store');
Route::post('translate', '\App\Http\Controllers\TranslationController@translate');
Route::get('languages', '\App\Http\Controllers\LanguageController@index');
Route::get('random-phrases/{count?}', '\App\Http\Controllers\PhraseController@getRandomPhrases');
