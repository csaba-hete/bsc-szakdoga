import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {appRoutes} from './app.routing';

import {RouterModule} from '@angular/router';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';

// interceptors
import {AuthInterceptor} from "./interceptors/auth.interceptor";
import {RequestOptionsInterceptor} from "./interceptors/request-options.interceptor";

// guards
import {AuthGuard} from './guards/auth.guard';
// services
import {AuthenticationService} from './services/authentication.service';
import {UserService} from './services/user.service';
import {SignUpService} from './services/sign-up.service';
import {NotificationService} from './services/notification.service';
import {LanguageService} from "./services/language.service";
import {TranslationService} from "./services/translation.service";
import {PhraseService} from "./services/phrase.service";
import {TestService} from "./services/test.service";

// components
import {AppComponent} from './app.component';
import {HomeComponent} from './components/home/home.component';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {LoginDialogComponent} from './components/dialogs/login/login-dialog.component';
import {RegisterDialogComponent} from './components/dialogs/register-dialog/register-dialog.component';
import {TestListComponent} from './components/tests/test-list/test-list.component';
import {GenerateTestDialogComponent} from "./components/dialogs/generate-test/generate-test-dialog.component";
import {TestFormComponent} from './components/tests/test-form/test-form.component';
import {DynamicQuizFormComponent} from './components/tests/dynamic-quiz-form/dynamic-quiz-form.component';
import {TestResultDialogComponent} from "./components/dialogs/test-result/test-result-dialog.component";
import { CreateTestComponent } from './components/tests/create-test/create-test.component';
import { ObjectKeysPipe } from './pipes/object-keys.pipe';
import { SearchHistoryComponent } from './components/search-history/search-history.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import {ChangePasswordDialogComponent} from "./components/dialogs/change-password-dialog/change-password-dialog.component";
import {ChangeEmailDialogComponent} from "./components/dialogs/change-email-dialog/change-email-dialog.component";
import { HangmanGameComponent } from './components/hangman-game/hangman-game.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    LoginDialogComponent,
    RegisterDialogComponent,
    TestListComponent,
    TestFormComponent,
    TestResultDialogComponent,
    GenerateTestDialogComponent,
    DynamicQuizFormComponent,
    CreateTestComponent,
    ObjectKeysPipe,
    SearchHistoryComponent,
    UserProfileComponent,
    ChangePasswordDialogComponent,
    ChangeEmailDialogComponent,
    HangmanGameComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    // material design modules
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatGridListModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatSortModule,
    MatMenuModule,
    // 3rd parties
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: RequestOptionsInterceptor, multi: true},
    AuthGuard,
    AuthenticationService,
    SignUpService,
    NotificationService,
    UserService,
    LanguageService,
    TranslationService,
    PhraseService,
    TestService,
    ObjectKeysPipe
  ],
  entryComponents: [
    GenerateTestDialogComponent,
    LoginDialogComponent,
    RegisterDialogComponent,
    TestResultDialogComponent,
    ChangePasswordDialogComponent,
    ChangeEmailDialogComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

