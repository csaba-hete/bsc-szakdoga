import {Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {TestListComponent} from "./components/tests/test-list/test-list.component";
import {TestFormComponent} from "./components/tests/test-form/test-form.component";
import {CreateTestComponent} from "./components/tests/create-test/create-test.component";
import {AuthGuard} from "./guards/auth.guard";
import {SearchHistoryComponent} from "./components/search-history/search-history.component";
import {UserProfileComponent} from "./components/user-profile/user-profile.component";
import {HangmanGameComponent} from "./components/hangman-game/hangman-game.component";

export const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'dictionary', component: HomeComponent },
  { path: 'tests', component: TestListComponent},
  { path: 'tests/create', component: CreateTestComponent, canActivate:[AuthGuard]},
  { path: 'tests/:test_id', component: TestFormComponent},
  { path: 'phrases', component: SearchHistoryComponent, canActivate:[AuthGuard]},
  { path: 'profile', component: UserProfileComponent, canActivate:[AuthGuard]},
  { path: 'hangman', component: HangmanGameComponent},

  // otherwise redirect to home
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];
