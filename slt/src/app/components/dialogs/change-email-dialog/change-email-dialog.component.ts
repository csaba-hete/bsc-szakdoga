import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {PasswordMatchValidation} from '../../../validators/password-match-validation';
import {NotificationService} from '../../../services/notification.service';
import {UserService} from "../../../services/user.service";
import {ChangeEmailDto} from "../../../dtos/change-email-dto";
import {HttpErrorResponse} from "@angular/common/http";


@Component({
  selector: 'app-change-email-dialog',
  templateUrl: './change-email-dialog.component.html',
  styleUrls: ['./change-email-dialog.component.css']
})
export class ChangeEmailDialogComponent implements OnInit {
  public form: FormGroup;
  private submitted: boolean;

  public buttonDisabled = false;

  constructor(private dialogRef: MatDialogRef<ChangeEmailDialogComponent>,
              private userService: UserService,
              private notificationService: NotificationService,) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', [
        Validators.email,
        Validators.required
      ]),
    });
  }

  get email() {
    return this.form.get('email');
  }

  get formError() {
    return this.submitted && !this.form.valid;
  }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public onSubmit(): void {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.userService.changeEmail(ChangeEmailDto.fromFormGroup(this.form))
        .subscribe(
          () => {
            this.dialogRef.close(true);
          },
          (err: HttpErrorResponse) => {
            this.buttonDisabled = false;
            switch (err.status) {
              case 412:
                this.notificationService.error('The given email address is invalid or already taken!');
                break;
              default:
                this.notificationService.error('Server error, try again later!');
            }
          }
        );
    }
  }

  public onCancel(): void {
    this.dialogRef.close(null);
  }
}
