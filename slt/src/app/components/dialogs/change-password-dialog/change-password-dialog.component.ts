import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {PasswordMatchValidation} from '../../../validators/password-match-validation';
import {NotificationService} from '../../../services/notification.service';
import {UserService} from "../../../services/user.service";
import {ChangePasswordDto} from "../../../dtos/change-password-dto";
import {HttpErrorResponse} from "@angular/common/http";


@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.css']
})
export class ChangePasswordDialogComponent implements OnInit {
  public form: FormGroup;
  private submitted: boolean;

  public passwordsAreTheSame = true;
  public buttonDisabled = false;

  constructor(private dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
              private userService: UserService,
              private notificationService: NotificationService,) {
  }

  ngOnInit() {
    this.form = new FormGroup({
      oldPassword: new FormControl('', [
        Validators.minLength(6),
        Validators.required
      ]),
      password: new FormControl('', [
        Validators.minLength(6),
        Validators.required
      ]),
      passwordConfirm: new FormControl('', [
        Validators.required,
      ]),
    }, PasswordMatchValidation.MatchPassword);
  }

  get oldPassword() {
    return this.form.get('oldPassword');
  }

  get password() {
    return this.form.get('password');
  }

  get passwordConfirm() {
    return this.form.get('passwordConfirm');
  }

  get formError() {
    return this.submitted && !this.form.valid;
  }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public onSubmit(): void {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.userService.changePassword(ChangePasswordDto.fromFormGroup(this.form))
        .subscribe(
          () => {
            this.dialogRef.close(true);
          },
          (err: HttpErrorResponse) => {
            this.buttonDisabled = false;
            switch (err.status) {
              case 412:
                this.notificationService.error('Old password is incorrect or the new password does not matches the requirements!');
                break;
              default:
                this.notificationService.error('Server error, try again later!');
            }
          }
        );
    }
  }

  public onCancel(): void {
    this.dialogRef.close(null);
  }
}
