import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../services/notification.service';
import {Language} from "../../../models/language.model";
import {LanguageService} from "../../../services/language.service";
import {DistinctTranslationLanguagesValidation} from "../../../validators/distinct-translation-languages-validation";
import {TestService} from "../../../services/test.service";
import {GenerateTestDto} from "../../../dtos/generate-test.dto";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-generate-test',
  templateUrl: './generate-test-dialog.component.html',
  styleUrls: ['./generate-test-dialog.component.css']
})

export class GenerateTestDialogComponent implements OnInit {
  public generateTestForm: FormGroup;
  public languages: Language[];
  public buttonDisabled = false;

  private submitted: boolean;
  private defaultFromLang = 'en';
  private defaultToLang = 'hu';

  constructor(@Inject(MAT_DIALOG_DATA) public data: Language[],
              private dialogRef: MatDialogRef<GenerateTestDialogComponent>) {
    this.generateTestForm = new FormGroup({
      fromLanguage: new FormControl('', [
        Validators.required
      ]),
      toLanguage: new FormControl('', [
        Validators.required
      ]),
    }, DistinctTranslationLanguagesValidation.DistinctLanguages);
  }

  ngOnInit() {
    this.languages = this.data;
    this.generateTestForm.controls['fromLanguage'].setValue(
      this.languages.find(lang => lang.language_code === this.defaultFromLang),
      {onlySelf: true}
    );
    this.generateTestForm.controls['toLanguage'].setValue(
      this.languages.find(lang => lang.language_code === this.defaultToLang),
      {onlySelf: true}
    );
  }

  get fromLanguage() {
    return this.generateTestForm.get('fromLanguage');
  }

  get toLanguage() {
    return this.generateTestForm.get('toLanguage');
  }

  get formError() {
    return this.submitted && !this.generateTestForm.valid;
  }

  public swapLangs() {
    const from = this.generateTestForm.controls['fromLanguage'].value;
    const to = this.generateTestForm.controls['toLanguage'].value;

    this.generateTestForm.controls['fromLanguage'].setValue(to, {onlySelf: true});
    this.generateTestForm.controls['toLanguage'].setValue(from, {onlySelf: true});
  }

  public onSubmit(): void {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.dialogRef.close(GenerateTestDto.fromFormGroup(this.generateTestForm))
    }
  }

  public onCancel(): void {
    this.dialogRef.close(null);
  }
}
