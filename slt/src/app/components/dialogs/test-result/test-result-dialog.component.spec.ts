import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {TestResultDialogComponent} from "./test-result-dialog.component";


describe('LoginDialogComponent', () => {
  let component: TestResultDialogComponent;
  let fixture: ComponentFixture<TestResultDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestResultDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestResultDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
