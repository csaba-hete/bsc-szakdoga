import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {NotificationService} from '../../../services/notification.service';
import {TestResults} from "../../../services/test.service";

@Component({
  selector: 'app-test-result',
  templateUrl: './test-result-dialog.component.html',
  styleUrls: ['./test-result-dialog.component.css']
})

export class TestResultDialogComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: TestResults,
              private dialogRef: MatDialogRef<TestResultDialogComponent>,
              private notificationService: NotificationService) {
  }

  public close(): void {
    this.dialogRef.close(null);
  }
}
