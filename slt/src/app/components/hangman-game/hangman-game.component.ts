import {Component, OnInit} from '@angular/core';
import {LanguageService} from "../../services/language.service";
import {NotificationService} from "../../services/notification.service";
import {PhraseService} from "../../services/phrase.service";
import {Phrase} from "../../models/phrase.model";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-hangman-game',
  templateUrl: './hangman-game.component.html',
  styleUrls: ['./hangman-game.component.css']
})
export class HangmanGameComponent implements OnInit {
  public phraseObject: Phrase;
  public alphabet: string[];
  public language: string;
  public text: string;
  public disabledLetters: object;
  public triesLeft: number;

  public dataLoaded = true;

  constructor(private languageService: LanguageService,
              private notificationService: NotificationService,
              private phraseService: PhraseService) {
  }

  ngOnInit() {
    this.newGame();
  }

  public tryLetter(letter: string) {
    if (this.triesLeft) {
      this.disabledLetters[letter] = true;
      if (!this.text.match(letter)) {
        --this.triesLeft;
      }
      this.notifyIfGameOver();
    } else {
      this.notificationService.error("The game is already over!");
    }
  }
  
  public newGame() {
    this.dataLoaded = false;
    this.disabledLetters = {};
    this.triesLeft = 10;
    this.phraseService.getRandomPhrases(100).subscribe(
      (result: Phrase[]) => {
        result = result.filter((phrase) => {
          return !!phrase.phrase.match('^[a-zA-Z\']{3,}$');
        });
        const rand = Math.floor(Math.random() * result.length);
        this.phraseObject = result[rand];
        this.alphabet = this.languageService.getAlphabet(this.phraseObject.language_code);
        this.language = LanguageService.languageLabels[this.phraseObject.language_code];
        this.text = this.phraseObject.phrase.toUpperCase();
        this.dataLoaded = true;
      },
      (err: HttpErrorResponse) => {
        this.dataLoaded = true;
        this.notificationService.error("Unexpected error while preparing the game!");
      }
    );
  }

  public isGuessed() {
    return this.text.split('').reduce((carry, letter) => carry && this.disabledLetters[letter], true);
  }

  private notifyIfGameOver() {
    if (this.isGuessed()) {
      this.notificationService.success("Congratulations! You have successfully guessed it!");
      return;
    }
    if (!this.triesLeft) {
      this.notificationService.warning("Sorry, maybe next time...");
      return;
    }
  }
}
