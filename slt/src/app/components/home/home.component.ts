import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../services/notification.service";
import {LanguageService} from "../../services/language.service";
import {Language} from "../../models/language.model";
import {TranslationService} from "../../services/translation.service";
import {TranslationDto} from "../../dtos/translation.dto";
import {HttpErrorResponse} from "@angular/common/http";
import {Phrase} from "../../models/phrase.model";
import {User} from "../../models/user.model";
import {AuthenticationService} from "../../services/authentication.service";
import {Subscription} from "rxjs/Subscription";
import {PhraseService} from "../../services/phrase.service";
import {SearchedPhraseDescriptor} from "../../models/searched-phrase-descriptor.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private static SEARCH_HISTORY_LIMIT = 20;

  public translationForm: FormGroup;
  public languages: Language[];

  public translations: Phrase[];
  public translationHistory: PreviousSearch[];

  private submitted: boolean;
  private buttonDisabled: boolean;
  private defaultFromLang = 'en';
  private defaultToLang = 'hu';
  private userLoginSubscription: Subscription;

  constructor(private notificationService: NotificationService,
              private languageService: LanguageService,
              private translationService: TranslationService,
              private authenticationService: AuthenticationService,
              private phraseService: PhraseService) {
    this.translationForm = new FormGroup({
      fromLanguage: new FormControl('', [
        Validators.required
      ]),
      toLanguage: new FormControl('', [
        Validators.required
      ]),
      phrase: new FormControl('', [
        Validators.required
      ])
    });
  }

  ngOnInit() {
    this.languageService.getLanguages()
      .subscribe(
        res => {
          this.languages = res;
          this.translationForm.controls['fromLanguage'].setValue(
            this.languages.find(lang => lang.language_code === this.defaultFromLang),
            {onlySelf: true}
          );
          this.translationForm.controls['toLanguage'].setValue(
            this.languages.find(lang => lang.language_code === this.defaultToLang),
            {onlySelf: true}
          );
        }
      );

    this.subscribeForUserAuthentication();
  }

  ngOnDestroy() {
    this.userLoginSubscription.unsubscribe();
  }

  private subscribeForUserAuthentication() {
    this.userLoginSubscription = this.authenticationService.loggedIn$.asObservable()
      .subscribe(
        (user?: User) => {
          if (user) {
            this.phraseService.getPhraseHistory(HomeComponent.SEARCH_HISTORY_LIMIT).subscribe(
              (descriptors: SearchedPhraseDescriptor[]) =>
                this.translationHistory = descriptors.map(
                  descriptor => new PreviousSearch(
                    descriptor.phrase.phrase,
                    descriptor.phrase.language_code,
                    descriptor.dst_lang
                  )
                )
            )
          } else {
            this.translationHistory = [];
          }
        }
      );
  }

  get fromLanguage() {
    return this.translationForm.get('fromLanguage');
  }

  get toLanguage() {
    return this.translationForm.get('toLanguage');
  }

  get phrase() {
    return this.translationForm.get('phrase');
  }

  get formError() {
    return this.submitted && !this.translationForm.valid;
  }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public swapLangs() {
    const from = this.translationForm.controls['fromLanguage'].value;
    const to = this.translationForm.controls['toLanguage'].value;

    this.translationForm.controls['fromLanguage'].setValue(to, {onlySelf: true});
    this.translationForm.controls['toLanguage'].setValue(from, {onlySelf: true});
  }

  public onSubmit() {
    this.submitted = true;

    const from = this.translationForm.controls['fromLanguage'].value;
    const to = this.translationForm.controls['toLanguage'].value;

    if (from === to) {
      this.notificationService.error("The source and the destination language must be different!");
      return;
    }

    if (!this.formError) {
      this.doTranslation();
    }
  }

  public translate(phrase: string, srcLang: string, dstLang: string) {
    this.translationForm.controls['fromLanguage'].setValue(this.languages.find(lang => lang.language_code === srcLang), {onlySelf: true});
    this.translationForm.controls['toLanguage'].setValue(this.languages.find(lang => lang.language_code === dstLang), {onlySelf: true});
    this.translationForm.controls['phrase'].setValue(phrase, {onlySelf: true});
    this.doTranslation();
  }

  private doTranslation() {
    this.buttonDisabled = true;
    this.translationService.translate(TranslationDto.fromFormGroup(this.translationForm))
      .subscribe(
        response => {
          this.buttonDisabled = false;
          this.notificationService.success(`${response.length} translations found`);
          this.translations = response;
          if (response.length) {
            this.appendLastSearchToHistory();
          }
        },
        (err: HttpErrorResponse) => {
          this.notificationService.error("Unexpected error while finding translation!");
          this.buttonDisabled = false;
        }
      );
  }

  private appendLastSearchToHistory() {
    const search = new PreviousSearch(
      this.translationForm.controls['phrase'].value,
      this.translationForm.controls['fromLanguage'].value.language_code,
      this.translationForm.controls['toLanguage'].value.language_code
    );
    if (this.translationHistory.reduce((c, a) => c && ((a.phrase !== search.phrase) || (a.srcLang !== search.srcLang || a.dstLang !== search.dstLang)), true)) {
      this.translationHistory.push(search);
    }
  }
}

class PreviousSearch {
  constructor(
    public phrase: string,
    public srcLang: string,
    public dstLang: string,
  ) {}
}