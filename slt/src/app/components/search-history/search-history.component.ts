import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {User} from "../../models/user.model";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {AuthenticationService} from "../../services/authentication.service";
import {Subscription} from "rxjs/Subscription";
import {HttpErrorResponse} from "@angular/common/http";
import {NotificationService} from "../../services/notification.service";
import {Router} from "@angular/router";
import {AuthGuard} from "../../guards/auth.guard";
import {SearchedPhraseDescriptor} from "../../models/searched-phrase-descriptor.model";
import {PhraseService} from "../../services/phrase.service";

@Component({
  selector: 'app-search-history',
  templateUrl: './search-history.component.html',
  styleUrls: ['./search-history.component.css']
})
export class SearchHistoryComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public dataLoaded = false;
  private userLoginSubscription: Subscription;

  public dataSource: MatTableDataSource<SearchedPhraseDescriptor>;
  public displayedColumns = ['translation_direction', 'phrase', 'last_tested_at', 'untrained_index'];

  constructor(private authenticationService: AuthenticationService,
              private authGuard: AuthGuard,
              private phraseService: PhraseService,
              private router: Router,
              private notificationService: NotificationService) {
    this.dataSource = new MatTableDataSource<SearchedPhraseDescriptor>([]);
  }

  ngOnInit(): void {
    this.subscribeForUserAuthentication();
    this.dataSource.filterPredicate =
      (data: SearchedPhraseDescriptor, filter: string) => !!data.phrase.phrase.toLowerCase().match(
        filter.toLowerCase()
      );
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.userLoginSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  public refresh() {
    this.dataLoaded = false;
    this.phraseService.getPhraseHistory().subscribe(
      response => {
        this.dataLoaded = true;
        this.dataSource.data = response
      },
      (err: HttpErrorResponse) => {
        this.dataLoaded = true;
        this.notificationService.error("Unexpected error while loading search history!");
      }
    );
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  private subscribeForUserAuthentication() {
    this.userLoginSubscription = this.authenticationService.loggedIn$.asObservable()
      .subscribe(
        (user?: User) => {
          if (!user) {
            this.router.navigate(['/'])
          } else {
            this.refresh();
          }
        }
      );
  }
}
