import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Language} from "../../../models/language.model";
import {LanguageService} from "../../../services/language.service";
import {PhraseService} from "../../../services/phrase.service";
import {HttpErrorResponse} from "@angular/common/http";
import {NotificationService} from "../../../services/notification.service";
import {Phrase} from "../../../models/phrase.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Test, TestService} from "../../../services/test.service";
import {CreateTestDto} from "../../../dtos/create-test.dto";

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.css']
})
export class CreateTestComponent implements OnInit {
  private defaultSrcLang = 'en';
  private defaultDstLang = 'hu';
  private dataLoaded = false;

  public srcLang: string;
  public dstLang: string;
  public phrases: object;
  public form: FormGroup;
  public buttonDisabled = false;
  public submitted = false;

  constructor(private route: ActivatedRoute,
              private phraseService: PhraseService,
              private notificationService: NotificationService,
              private testService: TestService,
              private router: Router,) {
  }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.srcLang = params.src_lang || this.defaultSrcLang;
        this.dstLang = params.dst_lang || this.defaultDstLang;
        this.getPhrasesInLanguage(this.srcLang);
        this.dataLoaded = true;
      });
  }

  get formError() {
    return this.submitted && !this.form.valid;
  }

  public onSubmit() {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.dataLoaded = false;
      this.testService.create(CreateTestDto.fromFormGroup(this.form)).subscribe(
        (test?: Test) => {
          this.notificationService.success("Test successfully saved!");
          this.router.navigate(['/tests/', test.id]);
          this.dataLoaded = true;
        },
        (err: HttpErrorResponse) => {
          switch (err.status){
            case 401:
              this.notificationService.error("You have no right for this operation!");
              break;
            case 412:
              this.notificationService.error("You haven't selected any phrases");
              break;
            default:
              this.notificationService.error("Unexpected error while saving the test!");
          }
          this.dataLoaded = true;
          this.buttonDisabled = false;
        }
      )
    }
  }

  public onCancel() {
    this.router.navigate(['/tests']);
  }

  private getPhrasesInLanguage(language: string): void {
    this.phraseService.getUserPhrases(language).subscribe(
      res => {
        if (!res.length) {
          this.notificationService.warning("You have appeared to have 0 searched phrase in the source language!");
          this.onCancel();
        } else {
          this.phrases = this.groupPhrasesAlphabetically(res);
          this.form = this.createFormGroups();
        }
      },
      (err: HttpErrorResponse) => {
        this.notificationService.error("Unexpected error when querying searched phrases!");
      },
    )
  }

  private groupPhrasesAlphabetically(phrases: Phrase[]): object {
    const grouped = {};
    phrases.forEach((phrase: Phrase) => {
      // const initialLetter = 'a';
      const initialLetter = phrase.phrase.trim().charAt(0);
      if (!grouped[initialLetter]) {
        grouped[initialLetter] = [phrase];
      } else {
        grouped[initialLetter].push(phrase)
      }
    });
    const ordered = {};
    Object.keys(grouped).sort().forEach(function (key) {
      ordered[key] = grouped[key];
    });
    return ordered;
  }

  private createFormGroups(): FormGroup {
    const group: any = {
      name: new FormControl('New Test', [
        Validators.minLength(3),
        Validators.maxLength(255),
        Validators.required
      ]),
      srcLang: new FormControl(this.srcLang),
      dstLang: new FormControl(this.dstLang),
      type: new FormControl('PUBLIC'),
    };
    const letters = Object.keys(this.phrases);
    for (let letter of letters) {
      for (let phrase of this.phrases[letter]) {
        group[phrase.id] = new FormControl('');
      }
    }
    return new FormGroup(group);
  }
}
