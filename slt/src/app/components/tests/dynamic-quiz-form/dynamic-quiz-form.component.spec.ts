import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicQuizFormComponent } from './dynamic-quiz-form.component';

describe('DynamicQuizFormComponent', () => {
  let component: DynamicQuizFormComponent;
  let fixture: ComponentFixture<DynamicQuizFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicQuizFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicQuizFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
