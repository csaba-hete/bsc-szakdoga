import {Component, Input} from '@angular/core';
import {Quiz} from "../../../services/test.service";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-dynamic-quiz-form',
  templateUrl: './dynamic-quiz-form.component.html',
  styleUrls: ['./dynamic-quiz-form.component.css']
})
export class DynamicQuizFormComponent {

  @Input() quiz: Quiz;

  @Input() i: number;

  @Input() form: FormGroup;

  @Input() submitted: boolean;

  get isValid() {
    return this.form.controls[this.quiz.id].valid;
  }

}
