import {Component, OnInit} from '@angular/core';
import {Quiz, Test, TestService} from "../../../services/test.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../../services/authentication.service";
import {NotificationService} from "../../../services/notification.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {EvaluateTestDto} from "../../../dtos/evaluate-test.dto";
import {MatDialog} from "@angular/material";
import {TestResultDialogComponent} from "../../dialogs/test-result/test-result-dialog.component";

@Component({
  selector: 'app-test-form',
  templateUrl: './test-form.component.html',
  styleUrls: ['./test-form.component.css']
})
export class TestFormComponent implements OnInit{
  public test: Test;
  public buttonDisabled = false;
  public submitted = false;
  public dataLoaded = false;
  public form: FormGroup;


  constructor(public authenticationService: AuthenticationService,
              private router: Router,
              private route: ActivatedRoute,
              private testService: TestService,
              private notificationService: NotificationService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('test_id');
    this.testService.get(id).subscribe(
      response => {
        this.dataLoaded = true;
        this.test = response;
        this.form = this.createFormGroups(this.test.quizzes);
      },
      (err: HttpErrorResponse) => {
        this.dataLoaded = true;
        switch (err.status) {
          case 404:
            this.notificationService.error("Test not found!");
            break;
          default:
            this.notificationService.error("Unexpected error while loading the test!");
        }
        this.onCancel();
      }
    )
  }

  get formError() {
    return this.submitted && !this.form.valid;
  }
  
  public onSubmit() {
    this.submitted = true;
    if (!this.formError) {
      this.buttonDisabled = true;
      this.testService.evaluate(EvaluateTestDto.fromFormGroup(this.form), this.test.id).subscribe(
        result => {
          const dialogRef = this.dialog.open(TestResultDialogComponent, {
            data: result,
          });

          dialogRef.afterClosed()
            .subscribe(
              result => {
                  this.router.navigate(['/tests']);
              },
            );
        },
        (err: HttpErrorResponse) => {
          this.buttonDisabled = false;
          this.dataLoaded = true;
          switch (err.status) {
            case 404:
              this.notificationService.error("Test not found!");
              this.onCancel();
              break;
            default:
              this.notificationService.error("Unexpected error while evaluating the test!");
          }
        }
      )
    }
  }

  public onCancel() {
    this.router.navigate(['/tests']);
  }

  private createFormGroups(quizzes: Quiz[]): FormGroup {
    const group: any = {};
    quizzes.forEach(
      (quiz: Quiz) => {
        group[quiz.id] = new FormControl('', [Validators.required])
      }
    );
    return new FormGroup(group);
  }
}
