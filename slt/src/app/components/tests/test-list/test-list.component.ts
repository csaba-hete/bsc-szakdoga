import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Test, TestService} from "../../../services/test.service";
import {Subscription} from "rxjs/Subscription";
import {AuthenticationService} from "../../../services/authentication.service";
import {User} from "../../../models/user.model";
import {Router} from "@angular/router";
import {NotificationService} from "../../../services/notification.service";
import {HttpErrorResponse} from "@angular/common/http";
import {GenerateTestDialogComponent} from "../../dialogs/generate-test/generate-test-dialog.component";
import {LanguageService} from "../../../services/language.service";
import {Language} from "../../../models/language.model";
import {GenerateTestDto} from "../../../dtos/generate-test.dto";
import {AuthGuard} from "../../../guards/auth.guard";

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.css']
})
export class TestListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public dataLoaded = false;
  public user: User;

  private userLoginSubscription: Subscription;
  private languages: Language[];

  public dataSource: MatTableDataSource<Test>;
  public displayedColumns = ['id', 'type', 'name', 'from_to', 'last_result', 'created_by', 'last_taken_at', 'actions'];

  constructor(private authenticationService: AuthenticationService,
              private authGuard: AuthGuard,
              private testService: TestService,
              private router: Router,
              private notificationService: NotificationService,
              private languageService: LanguageService,
              private dialog: MatDialog) {
    this.dataSource = new MatTableDataSource<Test>([]);

  }

  ngOnInit(): void {
    this.languageService.getLanguages()
      .subscribe(
        res => this.languages = res
      );
    this.subscribeForUserAuthentication();
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.userLoginSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  public refresh() {
    this.dataLoaded = false;
    this.testService.getTests().subscribe(
      response => {
        this.dataLoaded = true;
        this.dataSource.data = response
      },
      (err: HttpErrorResponse) => {
        this.dataLoaded = true;
        this.notificationService.error("Unexpected error while loading tests!");
      }
    );
  }

  public applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  private subscribeForUserAuthentication() {
    this.userLoginSubscription = this.authenticationService.loggedIn$.asObservable()
      .subscribe(
        (user?: User) => {
          this.user = user;
          this.refresh();
        }
      );
  }

  public take(test) {
    this.router.navigate(['/tests/', test.id]);
  }

  public generate() {
    const dialogRef = this.dialog.open(GenerateTestDialogComponent, {
      data: this.languages
    });

    dialogRef.afterClosed()
      .subscribe(
        (dto?: GenerateTestDto) => {
          if (dto) {
            this.dataLoaded = false;
            this.testService.generate(dto).subscribe(
              (test?: Test) => {
                this.dataLoaded = true;
                this.router.navigate(['/tests/', test.id]);
              },
              (err: HttpErrorResponse) => {
                this.dataLoaded = true;
                this.notificationService.error("Unexpected error while generating the test!");
              }
            )
          }
        },
      );
  }

  public create() {
    if (!this.authenticationService.isAuthenticated) {
      this.notificationService.error("Please sign in to use this functionality!");
      return;
    }

    const dialogRef = this.dialog.open(GenerateTestDialogComponent, {
      data: this.languages
    });

    dialogRef.afterClosed()
      .subscribe(
        (dto?: GenerateTestDto) => {
          if (dto) {
            this.router.navigate(['tests/create'], {queryParams: dto});
          }
        },
      );
  }

  public delete(test) {
    this.testService.delete(test.id).subscribe(
      response => {
        this.notificationService.success("Test successfully deleted")
        this.refresh();
      },
      (err: HttpErrorResponse) => {
        switch (err.status) {
          case 401:
          case 404:
            this.notificationService.error("Test not found or you are not the owner of it!");
            break;
          default:
            this.notificationService.error("Unexpected error while deleting the test!");
        }
      }
    );
  }
}