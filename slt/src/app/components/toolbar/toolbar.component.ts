import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {MatDialog} from '@angular/material';
import {User} from '../../models/user.model';
import {LoginDialogComponent} from '../dialogs/login/login-dialog.component';
import {NotificationService} from '../../services/notification.service';
import {RegisterDialogComponent} from '../dialogs/register-dialog/register-dialog.component';
import {AuthGuard} from '../../guards/auth.guard';
import {Subscription} from 'rxjs/Subscription';
import {UnauthorizedRouteData} from '../../guards/unauthorized-route-data';
import {Router} from '@angular/router';
import {UserService} from "../../services/user.service";
import {HttpErrorResponse, HttpResponse} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {Observable} from "rxjs/Observable";
import {UserLoginDto} from "../../dtos/user-login-dto";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  unauthorizedSubscription: Subscription;
  userLoginSubscription: Subscription;

  private currentUser: User;

  constructor(private authGuard: AuthGuard,
              private authenticationService: AuthenticationService,
              private userService: UserService,
              private router: Router,
              private notificationService: NotificationService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.subscribeForUnauthorizedRequests();
    this.subscribeForUserAuthentication();

    if (this.authenticationService.isAuthenticated) {
      this.userService.getCurrentUser().pipe(
        catchError(
          (err: HttpErrorResponse): Observable<User> => {
            switch (err.status) {
              default:
                this.notificationService.error("Your session has been expired!");
            }
            this.authenticationService.logout().subscribe(
              response => this.login()
            );
            return Observable.of(null);
          }
        )
      ).subscribe(
        (user: User) => {
          if (user) {
            this.authenticationService.publishLoggedInUser(user);
            this.currentUser = user;
          }
        },
      )
    }
  }

  ngOnDestroy() {
    this.unauthorizedSubscription.unsubscribe();
    this.userLoginSubscription.unsubscribe();
  }

  public login() {
    const dialogRef = this.dialog.open(LoginDialogComponent);

    dialogRef.afterClosed()
      .subscribe(
        (user?: User) => {
          if (user) {
            this.notificationService.success(`Welcome ${user.first_name} ${user.last_name}!`);
          }
        });
  }

  public logout() {
    this.authenticationService.logout()
      .subscribe(
        (response: HttpResponse<any>) => {
          this.notificationService.success('Signed out succesfuly!');
        },
        (err: HttpErrorResponse) => {
          switch (err.status) {
            default:
              this.notificationService.error('Error during logout!');
          }
        }
      );
  }

  public register() {
    const dialogRef = this.dialog.open(RegisterDialogComponent);

    dialogRef.afterClosed()
      .subscribe(
        (userLoginDto?: UserLoginDto) => {
          if (userLoginDto) {
            this.notificationService.success('Succesful registration!');
            this.authenticationService.login(userLoginDto)
              .subscribe(
                (user: User) => this.notificationService.success(`Welcome ${user.first_name} ${user.last_name}!`),
                (err: HttpErrorResponse) => {
                  switch (err.status) {
                    default:
                      this.notificationService.error('Error during login with freshly registered user!');
                  }
                },
              );
          }
        });
  }

  private subscribeForUserAuthentication() {
    this.userLoginSubscription = this.authenticationService.loggedIn$.asObservable()
      .subscribe(
        (user: User) => this.currentUser = user
      );
  }

  private subscribeForUnauthorizedRequests() {
    this.unauthorizedSubscription = this.authGuard.unauthorized$.asObservable()
      .subscribe(
        (data: UnauthorizedRouteData) => {
          const dialogRef = this.dialog.open(LoginDialogComponent);

          dialogRef.afterClosed()
            .subscribe(
              (result: User) => {
                if (result) {
                  this.notificationService.success('Signed in succesfully!');
                  this.router.navigate([data.url]);
                }
              });
        }
      );
  }
}
