import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from "../../models/user.model";
import {NotificationService} from "../../services/notification.service";
import {AuthenticationService} from "../../services/authentication.service";
import {Subscription} from "rxjs/Subscription";
import {Router} from "@angular/router";
import {AuthGuard} from "../../guards/auth.guard";
import {UserService} from "../../services/user.service";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material";
import {ChangePasswordDialogComponent} from "../dialogs/change-password-dialog/change-password-dialog.component";
import {ChangeEmailDialogComponent} from "../dialogs/change-email-dialog/change-email-dialog.component";
import {UserProfile} from "../../models/user-profile.model";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {PasswordMatchValidation} from "../../validators/password-match-validation";
import {UpdateUserDto} from "../../dtos/update-user.dto";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  private userLoginSubscription: Subscription;

  public dataLoaded = false;
  public data: UserProfile;
  public form: FormGroup;

  constructor(private authenticationService: AuthenticationService,
              private authGuard: AuthGuard,
              private userService: UserService,
              private router: Router,
              private notificationService: NotificationService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.subscribeForUserAuthentication();
    this.buildForm();
  }

  private buildForm() {
    this.form = new FormGroup({
      firstName: new FormControl(this.data ? this.data.first_name : '', [
        Validators.minLength(2),
        Validators.required
      ]),
      lastName: new FormControl(this.data ? this.data.last_name : '', [
        Validators.minLength(2),
        Validators.required
      ]),
    });
  }

  ngOnDestroy(): void {
    this.userLoginSubscription.unsubscribe();
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  public isInvalid(input: AbstractControl) {
    return input.dirty && input.errors;
  }

  public deleteAccount() {
    this.dataLoaded = false;
    this.userService.deleteAccount().subscribe(
      response => {
        this.dataLoaded = true;
        this.logout();
        this.notificationService.success("Account successfully deleted!");
      },
      (err: HttpErrorResponse) => {
        this.dataLoaded = true;
        this.notificationService.error("Unexpected error while deleting the account!");
      }
    )
  }

  private logout() {
    localStorage.removeItem(AuthenticationService.AUTH_TOKEN_STORAGE_KEY);
    this.authenticationService.publishLoggedInUser(null);
    this.router.navigate(['/']);
  }

  public changePassword() {
    const dialogRef = this.dialog.open(ChangePasswordDialogComponent);

    dialogRef.afterClosed()
      .subscribe(
        (result: boolean) => {
          if (result) {
            this.notificationService.success('Your password has been successfully changed!');
          }
        },
      );
  }

  public changeEmail() {
    const dialogRef = this.dialog.open(ChangeEmailDialogComponent);

    dialogRef.afterClosed()
      .subscribe(
        (result: boolean) => {
          if (result) {
            this.notificationService.success('A confirmation email was sent to your old email address!');
          }
        },
      );
  }

  public update() {
    if (this.form.valid) {
      this.dataLoaded = false;
      this.userService.update(UpdateUserDto.fromFormGroup(this.form)).subscribe(
        response => {
          this.dataLoaded = true;
          this.notificationService.success("Profile successfully updated!");
          this.data = response;
          this.buildForm();
        },
        (err: HttpErrorResponse) => {
          this.dataLoaded = true;
          switch (err.status) {
            case 412:
              this.notificationService.error('The given name is invalid!');
              break;
            default:
              this.notificationService.error('Unexpected error while updating user name!');
          }
        }
      );
    }
  }

  private refresh() {
    this.dataLoaded = false;
    this.userService.getProfileInfo().subscribe(
      response => {
        this.dataLoaded = true;
        this.data = response;
        this.buildForm();
      },
      (err: HttpErrorResponse) => {
        this.dataLoaded = true;
        this.notificationService.error("Unexpected error while loading user info!");
      }
    )
  }

  private subscribeForUserAuthentication() {
    this.userLoginSubscription = this.authenticationService.loggedIn$.asObservable()
      .subscribe(
        (user?: User) => {
          if (!user) {
            this.router.navigate(['/'])
          } else {
            this.refresh();
          }
        }
      );
  }
}
