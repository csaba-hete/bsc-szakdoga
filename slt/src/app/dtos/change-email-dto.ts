import { FormGroup } from '@angular/forms/src/model';

export class ChangeEmailDto {
    public static fromFormGroup(form: FormGroup) {
        return new ChangeEmailDto(
            form.controls['email'].value,
        );
    }

    constructor(
        public email: string,
    ) { }
}
