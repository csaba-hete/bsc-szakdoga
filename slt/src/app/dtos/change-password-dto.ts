import { FormGroup } from '@angular/forms/src/model';

export class ChangePasswordDto {
    public static fromFormGroup(form: FormGroup) {
        return new ChangePasswordDto(
            form.controls['oldPassword'].value,
            form.controls['password'].value,
        );
    }

    constructor(
        public old_password: string,
        public password: string,
    ) { }
}
