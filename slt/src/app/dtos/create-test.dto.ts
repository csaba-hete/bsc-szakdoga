import {FormGroup} from '@angular/forms/src/model';

export class CreateTestDto {
  public static fromFormGroup(form: FormGroup) {
    const name = form.controls['name'].value;
    const src_lang = form.controls['srcLang'].value;
    const dst_lang = form.controls['dstLang'].value;
    const type = form.controls['type'].value;
    const phrases = [];
    for (let key in form.value) {
      if (typeof key.match('^[0-9]+$') && form.value[key] === true) {
        phrases.push(key);
      }
    }
    return new CreateTestDto(
      name,
      src_lang,
      dst_lang,
      type,
      phrases
    );
  }

  constructor(public name: string,
              public src_lang: string,
              public dst_lang: string,
              public type: string,
              public phrases: number[],) {
  }
}
