import {FormGroup} from '@angular/forms/src/model';

export class EvaluateTestDto {
  public static fromFormGroup(form: FormGroup) {
    const quizzes = {};
    for (const prop in form.value) {
      if (form.value.hasOwnProperty(prop)) {
        quizzes[prop] = form.value[prop];
      }
    }
    return new EvaluateTestDto(quizzes);
  }

  constructor(public quizzes: {},) {
  }
}
