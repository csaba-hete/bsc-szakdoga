import {FormGroup} from '@angular/forms/src/model';

export class GenerateTestDto {
  public static fromFormGroup(form: FormGroup) {
    return new GenerateTestDto(
      form.controls['fromLanguage'].value.language_code,
      form.controls['toLanguage'].value.language_code,
    );
  }

  constructor(public src_lang,
              public dst_lang,) {
  }
}
