import { FormGroup } from '@angular/forms/src/model';

export class TranslationDto {
    public static fromFormGroup(form: FormGroup) {
        return new TranslationDto(
            form.controls['fromLanguage'].value.language_code,
            form.controls['toLanguage'].value.language_code,
            form.controls['phrase'].value,
        );
    }

    constructor(
        public src_lang,
        public dst_lang,
        public phrase,
    ) { }
}
