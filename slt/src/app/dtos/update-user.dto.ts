import { FormGroup } from '@angular/forms/src/model';

export class UpdateUserDto {
    public static fromFormGroup(form: FormGroup) {
        return new UpdateUserDto(
            form.controls['firstName'].value,
            form.controls['lastName'].value,
        );
    }

    constructor(
        public first_name: string,
        public last_name: string,
    ) { }
}
