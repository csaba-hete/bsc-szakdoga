import { FormGroup } from '@angular/forms/src/model';

export class UserLoginDto {
    public static fromFormGroup(form: FormGroup) {
        return new UserLoginDto(
            form.controls['email'].value,
            form.controls['password'].value,
        );
    }

    constructor(
        public email,
        public password
    ) { }
}
