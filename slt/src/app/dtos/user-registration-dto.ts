import { FormGroup } from '@angular/forms/src/model';

export class UserRegistrationDto {
    public static fromFormGroup(form: FormGroup) {
        return new UserRegistrationDto(
            form.controls['firstName'].value,
            form.controls['lastName'].value,
            form.controls['email'].value,
            form.controls['username'].value,
            form.controls['password'].value,
        );
    }

    constructor(
        public first_name: string,
        public last_name: string,
        public email: string,
        public username: string,
        public password: string,
    ) { }
}
