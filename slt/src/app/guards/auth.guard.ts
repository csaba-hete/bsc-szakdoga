import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Subject } from 'rxjs/Subject';
import { UnauthorizedRouteData } from './unauthorized-route-data';
import {NotificationService} from "../services/notification.service";

@Injectable()
export class AuthGuard implements CanActivate {
  public unauthorized$ = new Subject<UnauthorizedRouteData>();

  constructor(
    public authenticationService: AuthenticationService,
    public notificationService: NotificationService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem(AuthenticationService.AUTH_TOKEN_STORAGE_KEY) || this.authenticationService.isAuthenticated) {
      return true;
    }

    this.notificationService.warning("You have to sign in to use this functionality!");
    this.unauthorized$.next(new UnauthorizedRouteData(state.url));
    return false;
  }
}
