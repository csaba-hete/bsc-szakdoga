import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpEventType} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {AuthenticationService} from "../services/authentication.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<HttpEventType.Response>> {
    const authToken = this.authService.accessToken;
    if (authToken) {
      const authReq = req.clone({
        setHeaders: {
          'Authorization': `Bearer ${authToken}`,
        }
      });
      return next.handle(authReq);
    }
    return next.handle(req);
  }

}