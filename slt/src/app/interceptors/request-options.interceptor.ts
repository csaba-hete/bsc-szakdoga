import {Injectable} from '@angular/core';
import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RequestOptionsInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<HttpEventType.Response>> {
    const modifiedReq = req.clone({
      setHeaders: {
        'Content-Type': 'application/json; charset=utf-8',
      }
    });
    return next.handle(modifiedReq);
  }

}