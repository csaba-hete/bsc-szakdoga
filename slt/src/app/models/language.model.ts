export interface Language {
  id: number,
  name: string,
  language_code: string,
  created_at: string,
  updated_at: string,
}
