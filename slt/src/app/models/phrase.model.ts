export interface Phrase {
  id: number,
  language_code: string,
  phrase: string,
  created_at: string,
  updated_at: string,
}
