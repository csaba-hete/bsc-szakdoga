import {Phrase} from "./phrase.model";

export interface SearchedPhraseDescriptor {
  id: number,
  user_id: number,
  phrase_id: number,
  dst_lang: string,
  untrained_index: number,
  created_at: string,
  updated_at: string,

  phrase?: Phrase
}
