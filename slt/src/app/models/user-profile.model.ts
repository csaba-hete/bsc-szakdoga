export interface UserProfile {
  id: number,
  first_name: string,
  last_name: string,
  full_name: string,
  email: string,
  username: string,
  created_at: string,
  updated_at: string,

  tests_taken: number,
  avg_test_result:number,
  phrases_searched: number,
  phrases_learned: number,
  avg_phrase_untrained_index:number,
}
