export class ErrorResponse {

  constructor(public id: string,
              public error: string,
              public details: string,
              public status: number,) {
  }
}
