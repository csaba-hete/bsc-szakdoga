import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../models/user.model';
import {UserLoginDto} from '../dtos/user-login-dto';

import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {flatMap, map, tap} from 'rxjs/operators';
import {UserService} from './user.service';
import {RestResponse} from "../responses/rest.response";
import {Router} from "@angular/router";

@Injectable()
export class AuthenticationService {
  public static AUTH_TOKEN_STORAGE_KEY = 'auth_token';

  private _accessToken: AccessToken;
  private loggedIn: boolean;

  public loggedIn$ = new BehaviorSubject<User>(null);

  constructor(private userService: UserService,
              private http: HttpClient,
              private router: Router,) {
    const authToken = localStorage.getItem(AuthenticationService.AUTH_TOKEN_STORAGE_KEY);
    if (authToken) {
      this._accessToken = JSON.parse(authToken);
    }
  }

  public login(userLoginDto: UserLoginDto): Observable<User> {
    return this.authenticate(userLoginDto).pipe(
      flatMap(
        res => this.userService.getCurrentUser(true).pipe(
          tap(
            user => this.publishLoggedInUser(user)
          )
        ),
      )
    )
  }

  public logout(): Observable<any> {
    return this.http.post('/api/auth/logout', {})
      .pipe(
        tap(() => {
          localStorage.removeItem(AuthenticationService.AUTH_TOKEN_STORAGE_KEY);
          this.publishLoggedInUser(null);
          this._accessToken = null;
          this.router.navigate(['/']);
        }),
      )
  }

  public publishLoggedInUser(user?: User) {
    this.loggedIn = !!user;
    this.loggedIn$.next(user);
  }

  get isAuthenticated(): boolean {
    return !!this._accessToken;
  }

  get accessToken() {
    return this._accessToken ? this._accessToken.access_token : null;
  }

  private authenticate(userLoginDto: UserLoginDto): Observable<AccessToken> {
    return this.http.post<RestResponse<AccessToken>>('/api/auth/login', userLoginDto)
      .pipe(
        map(
          response => response.data
        ),
        tap(
          response => this.setAccessToken(response)
        ),
      );
  }

  private setAccessToken(accessToken: AccessToken) {
    this._accessToken = accessToken;
    localStorage.setItem(AuthenticationService.AUTH_TOKEN_STORAGE_KEY, JSON.stringify(accessToken));
  }
}

interface AccessToken {
  access_token: string,
  token_type: string,
  expires_in: number
}
