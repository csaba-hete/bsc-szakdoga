import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Language} from "../models/language.model";
import {RestResponse} from "../responses/rest.response";
import {map, tap} from "rxjs/operators";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/of";

@Injectable()
export class LanguageService {

  public static languageLabels = {
    hu: 'Hungarian',
    en: 'English',
  };

  private languages: Language[];
  private alphabets = {
    hu: [
      'A', 'Á', 'B', 'C', 'D', 'E', 'É', 'F', 'G',
      'H', 'I', 'Í', 'J', 'K', 'L', 'M', 'N', 'O',
      'Ó', 'Ö', 'Ő', 'P', 'Q', 'R', 'S', 'T', 'U',
      'Ú', 'Ü', 'Ű', 'V', 'W', 'X', 'Y', 'Z', '\''
    ],
    en: [
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
      'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
      'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '\''
    ]
  };

  constructor(private http: HttpClient) {
  }

  public getLanguages(force?: boolean): Observable<Language[]> {
    return this.languages && !force
      ? Observable.of(this.languages)
      : this.http.get<RestResponse<Language[]>>('/api/languages')
        .pipe(
          map(
            response => response.data || []
          ),
          tap(
            response => this.languages = response
          )
        )
  }

  public getAlphabet(language: string): string[] {
    return this.alphabets[language];
  }
}
