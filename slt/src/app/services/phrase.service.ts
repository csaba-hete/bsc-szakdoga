import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RestResponse} from "../responses/rest.response";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";
import {Phrase} from "../models/phrase.model";
import {SearchedPhraseDescriptor} from "../models/searched-phrase-descriptor.model";

@Injectable()
export class PhraseService {

  constructor(private http: HttpClient) {
  }

  public getPhraseHistory(count?: number): Observable<SearchedPhraseDescriptor[]> {
    return this.http.get<RestResponse<SearchedPhraseDescriptor[]>>(`/api/user/phrases/history/${count || ''}`)
      .pipe(
        map(
          response => response.data as SearchedPhraseDescriptor[] || []
        ),
      );
  }

  public getUserPhrases(language: string): Observable<Phrase[]> {
    return this.http.get<RestResponse<Phrase[]>>(`/api/user/phrases?language=${language}`)
      .pipe(
        map(
          response => response.data as Phrase[] || []
        ),
      );
  }

  public getRandomPhrases(count?: number): Observable<Phrase[]> {
    return this.http.get<RestResponse<Phrase[]>>(`/api/random-phrases/${count || ''}`)
      .pipe(
        map(
          response => response.data as Phrase[] || []
        ),
      );
  }
}
