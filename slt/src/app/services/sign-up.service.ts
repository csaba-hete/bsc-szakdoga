import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {UserRegistrationDto} from '../dtos/user-registration-dto';
import {RestResponse} from "../responses/rest.response";

@Injectable()
export class SignUpService {

  constructor(private http: HttpClient) {
  }

  public register(userRegistrationDto: UserRegistrationDto): Observable<any> {
    return this.http.post<RestResponse<any>>('/api/user', userRegistrationDto)
  }
}
