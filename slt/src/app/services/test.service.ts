import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RestResponse} from "../responses/rest.response";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";
import {Phrase} from "../models/phrase.model";
import {GenerateTestDto} from "../dtos/generate-test.dto";
import {EvaluateTestDto} from "../dtos/evaluate-test.dto";
import {CreateTestDto} from "../dtos/create-test.dto";

@Injectable()
export class TestService {

  constructor(private http: HttpClient) {
  }

  public getTests(): Observable<Test[]> {
    return this.http.get<RestResponse<Test[]>>('/api/tests')
      .pipe(
        map(
          response => response.data as Test[] || []
        ),
      );
  }

  public create(dto: CreateTestDto): Observable<Test> {
    return this.http.post<RestResponse<Test>>('/api/tests', dto)
      .pipe(
        map(
          response => response.data as Test
        ),
      );
  }

  public get(id: number): Observable<Test> {
    return this.http.get<RestResponse<Test>>(`/api/tests/${id}`)
      .pipe(
        map(
          response => response.data as Test
        ),
      );
  }

  public generate(generateTestDto: GenerateTestDto): Observable<Test> {
    return this.http.post<RestResponse<Test>>('/api/tests/generate', generateTestDto)
      .pipe(
        map(
          response => response.data as Test
        ),
      );
  }
  
  public evaluate(evaluateTestDto: EvaluateTestDto, test_id: number) {
    return this.http.post<RestResponse<TestResults>>(`/api/tests/${test_id}/evaluate`, evaluateTestDto)
      .pipe(
        map(
          response => response.data as TestResults
        ),
      );
  }

  public delete(id: number): Observable<any> {
    return this.http.delete<RestResponse<any>>(`/api/tests/${id}`)
      .pipe(
        map(
          response => response.data
        ),
      );
  }
}

export interface Quiz {
  id: number;
  phrase: Phrase;
  choices: Phrase[];
}

export interface Test {
  id: number;
  name: string;
  from_language: string;
  to_language: string;
  type: string;
  owner_id: number;
  created_at: string;
  updated_at: string;

  quizzes?: Quiz[];
  last_resolution_ratio?: number;
  last_taken_at?: string;
}

export interface TestResults {
  correct: TestResult[];
  incorrect: TestResult[];
}

interface TestResult {
  phrase: string;
  answer: string;
}