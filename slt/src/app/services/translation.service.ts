import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TranslationDto} from "../dtos/translation.dto";
import {Observable} from "rxjs/Observable";
import {Phrase} from "../models/phrase.model";
import {RestResponse} from "../responses/rest.response";
import {map} from "rxjs/operators";

@Injectable()
export class TranslationService {
  constructor(private http: HttpClient) { }

  public translate(translationDto: TranslationDto): Observable<Phrase[]> {
    return this.http.post<RestResponse<Phrase[]>>('/api/translate', translationDto)
      .pipe(
        map(
          response => response.data || []
        )
      );
  }
}
