import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';
import {RestResponse} from "../responses/rest.response";
import {map} from "rxjs/operators";
import {UserProfile} from "../models/user-profile.model";
import {ChangePasswordDto} from "../dtos/change-password-dto";
import {ChangeEmailDto} from "../dtos/change-email-dto";
import {UpdateUserDto} from "../dtos/update-user.dto";

@Injectable()
export class UserService {
  private user?: User;
  constructor(private http: HttpClient) {
  }

  public getCurrentUser(force?: boolean): Observable<User> {
    if (force || !this.user) {
      return this.http.get<RestResponse<User>>('/api/user')
        .pipe(
          map(
            response => response.data as User
          ),
        );
    }

    return Observable.of(this.user);
  }

  public getProfileInfo(): Observable<UserProfile>  {
    return this.http.get<RestResponse<UserProfile>>('/api/user/details')
      .pipe(
        map(
          response => response.data as UserProfile
        ),
      );
  }

  public deleteAccount(): Observable<any> {
    return this.http.delete<RestResponse<any>>('/api/user')
      .pipe(
        map(
          response => response.data
        ),
      );
  }

  public changePassword(dto: ChangePasswordDto): Observable<any> {
    return this.http.post<RestResponse<any>>('/api/user/change-password', dto)
      .pipe(
        map(
          response => response.data
        ),
      );
  }

  public changeEmail(dto: ChangeEmailDto): Observable<any> {
    return this.http.post<RestResponse<any>>('/api/user/change-email', dto)
      .pipe(
        map(
          response => response.data
        ),
      );
  }

  public update(dto: UpdateUserDto): Observable<UserProfile> {
    return this.http.put<RestResponse<UserProfile>>('/api/user', dto)
      .pipe(
        map(
          response => response.data as UserProfile
        ),
      );
  }
}
