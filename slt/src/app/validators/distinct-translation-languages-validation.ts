import { FormGroup } from '@angular/forms';
export class DistinctTranslationLanguagesValidation {

    static DistinctLanguages(g: FormGroup) {
      const fromLanguage = g.get('fromLanguage');
      const toLanguage = g.get('toLanguage');

      const unsetDistinctLanguagesError = (errors?: any) =>  {
          if (errors && errors.hasOwnProperty("distinctLanguages")) {
              delete errors["distinctLanguages"];
          }
          return errors && Object.getOwnPropertyNames(errors).length > 0 ? errors : null;
      };

      const distinct = fromLanguage.value !== toLanguage.value;
        if (!distinct) {
            fromLanguage.setErrors({distinctLanguages: false});
            toLanguage.setErrors({distinctLanguages: false});
        } else {
            fromLanguage.setErrors(unsetDistinctLanguagesError(fromLanguage.errors));
            toLanguage.setErrors(unsetDistinctLanguagesError(toLanguage.errors));
        }
        return distinct ? null : { 'distinctLanguages': true };
    }
}
