<?php

namespace Tests\Functional;

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LanguageControllerTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testIndex()
    {
        //Arrange

        //Act
        $response = $this->get('/api/languages');

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(["language_code" => "hu"]);
        $response->assertJsonFragment(["language_code" => "en"]);
        $response->assertJsonFragment(["name" => "Hungarian"]);
        $response->assertJsonFragment(["name" => "English"]);
    }
}
