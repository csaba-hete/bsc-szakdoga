<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PhraseController_GuestUserTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testGetRandomPhrases()
    {
        //Arrange

        //Act
        $response = $this->get('/api/random-phrases');

        //Assert
        $response->assertStatus(200);
    }

    public function testGetNRandomPhrases()
    {
        //Arrange
        $phraseCount = 10;

        //Act
        $response = $this->get("/api/random-phrases/$phraseCount");

        //Assert
        $response->assertStatus(200);
        $phrases = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(count($phrases) === $phraseCount);
    }

    public function testGetUserPhrases_unauthorized_shouldFail()
    {
        //Arrange
        $language = 'hu';

        //Act
        $response = $this->get("/api/user/phrases?language=$language");

        //Assert
        $response->assertStatus(401);
    }

    public function testGetPhraseHistory_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get('/api/user/phrases/history');

        //Assert
        $response->assertStatus(401);
    }
}
