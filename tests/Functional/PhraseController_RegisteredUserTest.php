<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestWithLoggedInUser;

class PhraseController_RegisteredUserTest extends TestWithLoggedInUser
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testGetRandomPhrases()
    {
        //Arrange

        //Act
        $response = $this->get('/api/random-phrases', $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
    }

    public function testGetNRandomPhrases()
    {
        //Arrange
        $phraseCount = 10;

        //Act
        $response = $this->get("/api/random-phrases/$phraseCount", $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $phrases = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(count($phrases) === $phraseCount);
    }

    public function testGetUserPhrases()
    {
        //Arrange
        $language = 'hu';

        //Act
        $response = $this->get("/api/user/phrases?language=$language", $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $phrases = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(count($phrases) === 0);
    }

    public function testGetUserPhrases_noLanguageParam_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get("/api/user/phrases", $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testGetUserPhrases_invalidLanguage_shouldFail()
    {
        //Arrange
        $language = 'asdf';

        //Act
        $response = $this->get("/api/user/phrases?language=$language", $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testGetSearchHistory_emptyHistory()
    {
        //Arrange

        //Act
        $response = $this->get('/api/user/phrases/history', $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $phrases = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(count($phrases) === 0);
    }
}
