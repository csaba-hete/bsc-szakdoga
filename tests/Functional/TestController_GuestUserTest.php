<?php

namespace Tests\Functional;

use App\Enums\LanguageEnum;
use App\Enums\TestTypeEnum;
use App\Models\Quiz;
use App\Models\Test;
use App\Services\TestService;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TestController_GuestUserTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testIndex()
    {
        //Arrange

        //Act
        $response = $this->get('/api/tests');

        //Assert
        $response->assertStatus(200);
    }

    public function testGenerate()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];

        //Act
        $response = $this->post('/api/tests/generate', $generateTestDto);

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertJsonFragment(['from_language' => $srcLang]);
        $response->assertJsonFragment(['to_language' => $dstLang]);
        $response->assertJsonFragment(['name' => 'Generated test']);
        $response->assertSee('quizzes');

        $testId = json_decode($response->getContent(), true)['data']['id'];
        $quizzes = json_decode($response->getContent(), true)['data']['quizzes'];
        $this->assertTrue(count($quizzes) === TestService::TEST_QUIZ_COUNT);

        $this->assertDatabaseHas('tests', ['name' => 'Generated test']);
        $this->assertTrue(Quiz::where('test_id', '=', $testId)->count() === TestService::TEST_QUIZ_COUNT);
    }

    public function testGenerate_invalidSrcLanguage()
    {
        //Arrange
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => 'asdfasdf',
            'dst_lang' => $dstLang,
        ];

        //Act
        $response = $this->post('/api/tests/generate', $generateTestDto);

        //Assert
        $response->assertStatus(412);
    }

    public function testGenerate_missingSrcLanguage()
    {
        //Arrange
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'dst_lang' => $dstLang,
        ];

        //Act
        $response = $this->post('/api/tests/generate', $generateTestDto);

        //Assert
        $response->assertStatus(412);
    }

    public function testGenerate_invalidDstLanguage()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => 'asdfasdf',
        ];

        //Act
        $response = $this->post('/api/tests/generate', $generateTestDto);

        //Assert
        $response->assertStatus(412);
    }

    public function testGenerate_missingDstLanguage()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
        ];

        //Act
        $response = $this->post('/api/tests/generate', $generateTestDto);

        //Assert
        $response->assertStatus(412);
    }

    public function testShow()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];
        $response = $this->post('/api/tests/generate', $generateTestDto);
        $testId = json_decode($response->getContent(), true)['data']['id'];

        //Act
        $response = $this->get("/api/tests/$testId");

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertJsonFragment(['from_language' => $srcLang]);
        $response->assertJsonFragment(['to_language' => $dstLang]);
        $response->assertJsonFragment(['name' => 'Generated test']);
        $response->assertSee('quizzes');
    }

    public function testShow_privateTest_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];
        $response = $this->post('/api/tests/generate', $generateTestDto);
        $testId = json_decode($response->getContent(), true)['data']['id'];
        $test = Test::find($testId);
        $test->type = TestTypeEnum::PRIVATE()->getConstName();
        $test->save();

        //Act
        $response = $this->get("/api/tests/$testId");

        //Assert
        $response->assertStatus(404);
    }

    public function testShow_testNotExists_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get('/api/tests/0');

        //Assert
        $response->assertStatus(404);
    }

    public function testDestroy()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];
        $response = $this->post('/api/tests/generate', $generateTestDto);
        $testId = json_decode($response->getContent(), true)['data']['id'];

        //Act
        $response = $this->delete("/api/tests/$testId");

        //Assert
        $response->assertStatus(401);
    }

    public function testStore()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $storeTestDto = [
            'name' => 'required|string|between:3,255',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => 'PUBLIC',
            'phrases' => [
                1,2,3,4
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto);

        //Assert
        $response->assertStatus(401);
    }

    public function testEvaluate()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];
        $response = $this->post('/api/tests/generate', $generateTestDto);
        $testId = json_decode($response->getContent(), true)['data']['id'];
        $quizzes = json_decode($response->getContent(), true)['data']['quizzes'];
        $evaluateTestDto = $this->getEvaluateQuizzesDto($quizzes);

        //Act
        $response = $this->post("/api/tests/$testId/evaluate", $evaluateTestDto);

        //Assert
        $response->assertStatus(200);
        $response->assertSee('correct');
        $response->assertSee('incorrect');
    }

    public function testEvaluate_privateTest_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];
        $response = $this->post('/api/tests/generate', $generateTestDto);
        $testId = json_decode($response->getContent(), true)['data']['id'];
        $test = Test::find($testId);
        $test->type = TestTypeEnum::PRIVATE()->getConstName();
        $test->save();

        $quizzes = json_decode($response->getContent(), true)['data']['quizzes'];
        $evaluateTestDto = $this->getEvaluateQuizzesDto($quizzes);

        //Act
        $response = $this->post("/api/tests/$testId/evaluate", $evaluateTestDto);

        //Assert
        $response->assertStatus(404);
    }

    public function testEvaluate_notExistingTest_shouldFail()
    {
        //Arrange
        $evaluateTestDto = [
            'quizzes' => [
                1 => 23
            ]
        ];

        //Act
        $response = $this->post("/api/tests/0/evaluate", $evaluateTestDto);

        //Assert
        $response->assertStatus(404);
    }

    public function testEvaluate_emptyAnswerArray_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];
        $response = $this->post('/api/tests/generate', $generateTestDto);
        $testId = json_decode($response->getContent(), true)['data']['id'];
        $evaluateTestDto = $this->getEvaluateQuizzesDto([]);

        //Act
        $response = $this->post("/api/tests/$testId/evaluate", $evaluateTestDto);

        //Assert
        $response->assertStatus(412);
    }

    private function getEvaluateQuizzesDto(array $quizzes): array
    {
        $ret = [];
        foreach ($quizzes as $quiz) {
            $choices = array_column($quiz['choices'], 'id');
            $ret[$quiz['id']] = $choices[array_rand($choices)];
        }
        return ['quizzes' => $ret];
    }
}
