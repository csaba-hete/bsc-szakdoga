<?php

namespace Tests\Functional;

use App\Enums\LanguageEnum;
use App\Enums\TestTypeEnum;
use App\Models\Phrase;
use App\Models\Quiz;
use App\Models\Test;
use App\Models\Translation;
use App\Services\TestService;
use App\Utils\Translators\ExternalTranslator;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestWithLoggedInUser;

class TestController_RegisteredUserTest extends TestWithLoggedInUser
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testGenerate()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $generateTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
        ];

        //Act
        $response = $this->post('/api/tests/generate', $generateTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertJsonFragment(['from_language' => $srcLang]);
        $response->assertJsonFragment(['to_language' => $dstLang]);
        $response->assertJsonFragment(['name' => 'Generated test']);
        $response->assertSee('quizzes');
        $response->assertJsonFragment(['phrase' => $phrase]);

        $testId = json_decode($response->getContent(), true)['data']['id'];
        $quizzes = json_decode($response->getContent(), true)['data']['quizzes'];
        $this->assertTrue(count($quizzes) === TestService::TEST_QUIZ_COUNT);

        $this->assertDatabaseHas('tests', ['name' => 'Generated test']);
        $this->assertTrue(Quiz::where('test_id', '=', $testId)->count() === TestService::TEST_QUIZ_COUNT);
        $this->assertTrue($user->testsOwned->count() === 0);
        $this->assertTrue($user->testsTaken->count() === 0);
    }

    public function testStorePublicTest()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
                'phrase1_id' => $hungarianPhrase->id,
                'phrase2_id' => $englishPhrase->id,
            ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom public test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PUBLIC()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertJsonFragment(['from_language' => $srcLang]);
        $response->assertJsonFragment(['to_language' => $dstLang]);
        $response->assertJsonFragment(['name' => 'Custom public test']);

        $testId = json_decode($response->getContent(), true)['data']['id'];

        $this->assertDatabaseHas('tests', ['name' => 'Custom public test']);
        $this->assertTrue(Quiz::where('test_id', '=', $testId)->count() === count($storeTestDto['phrases']));
        $this->assertTrue($user->testsOwned->count() === 1);
        $this->assertTrue($user->testsTaken->count() === 0);
    }

    public function testStorePrivateTest()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertJsonFragment(['from_language' => $srcLang]);
        $response->assertJsonFragment(['to_language' => $dstLang]);
        $response->assertJsonFragment(['name' => 'Custom private test']);

        $testId = json_decode($response->getContent(), true)['data']['id'];

        $this->assertDatabaseHas('tests', ['name' => 'Custom private test']);
        $this->assertTrue(Quiz::where('test_id', '=', $testId)->count() === count($storeTestDto['phrases']));
        $this->assertTrue($user->testsOwned->count() === 1);
        $this->assertTrue($user->testsTaken->count() === 0);
    }

    public function testStore_emptyPhraseArray_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_invalidSrcLang_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => 'asdfasdf',
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_emptySrcLang_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_invalidDstLang_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'dst_lang' => 'asdfasdf',
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_emptyDstLang_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_emptyName_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => '',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PUBLIC()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_missingName_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PUBLIC()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_invalidType_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => 'afsdaffd',
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testStore_missingType_shouldFail()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];

        //Act
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testShowOwnPrivateTest()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());
        $testId = json_decode($response->getContent(), true)['data']['id'];

        //Act
        $response = $this->get("/api/tests/$testId", $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertSee('id');
        $response->assertJsonFragment(['from_language' => $srcLang]);
        $response->assertJsonFragment(['to_language' => $dstLang]);
        $response->assertJsonFragment(['name' => 'Custom private test']);
        $response->assertSee('quizzes');
    }

    public function testEvaluateTest_correctAnswer()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        $testId = json_decode($response->getContent(), true)['data']['id'];
        $quiz = Test::find($testId)->quizzes->first();
        $evaluateTestDto = [
            'quizzes' => [
                $quiz->id => $quiz->solution_id,
            ],
        ];

        //Act
        $response = $this->post("/api/tests/$testId/evaluate", $evaluateTestDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertSee('correct');
        $response->assertSee('incorrect');
        $this->assertTrue($user->testsTaken->count() === 1);
        $this->assertTrue($user->searchedPhraseDescriptors->first()->fresh()->untrained_index === 4);
    }

    public function testEvaluateTest_incorrectAnswer()
    {
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $user = $this->getUser();

        // save a translation to the user
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translationDto = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];
        $response = $this->post('/api/translate', $translationDto, $this->getAuthorizationHeader());

        $storeTestDto = [
            'name' => 'Custom private test',
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'type' => TestTypeEnum::PRIVATE()->getConstName(),
            'phrases' => [
                $user->searchedPhraseDescriptors->first()->phrase->id
            ],
        ];
        $response = $this->post('/api/tests', $storeTestDto, $this->getAuthorizationHeader());

        $testId = json_decode($response->getContent(), true)['data']['id'];
        $quiz = Test::find($testId)->quizzes->first();
        $evaluateTestDto = [
            'quizzes' => [
                $quiz->id => $quiz->translatable_id,
            ],
        ];

        //Act
        $response = $this->post("/api/tests/$testId/evaluate", $evaluateTestDto, $this->getAuthorizationHeader());
        //Assert
        $response->assertStatus(200);
        $response->assertSee('correct');
        $response->assertSee('incorrect');
        $this->assertTrue($user->testsTaken->count() === 1);
        $this->assertTrue($user->searchedPhraseDescriptors->first()->fresh()->untrained_index === 6);
    }
}
