<?php

namespace Tests\Functional;

use App\Enums\LanguageEnum;
use App\Models\Phrase;
use App\Models\PhraseUserPivot;
use App\Models\Translation;
use App\Utils\Translators\ExternalTranslator;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TranslationController_GuestUserTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testGetTranslation_phraseExistsInDatabase()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);

        $phraseUserPivotCount = PhraseUserPivot::count();
        $translation = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['phrase' => $phrase]);
        $this->assertTrue($phraseUserPivotCount === PhraseUserPivot::count());
    }

    public function testGetTranslation_phraseNotExistsInDatabase()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phraseUserPivotCount = PhraseUserPivot::count();
        $phraseCount = Phrase::count();
        $translationCount = Translation::count();

        $specialPhrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $this->assertTrue(Phrase::where([
                ['phrase', '=', $specialPhrase],
                ['language_code', '=', $srcLang],
            ])->count() === 0);

        $translation = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $specialPhrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['phrase' => $specialPhrase]);
        $this->assertTrue($phraseUserPivotCount === PhraseUserPivot::count());
        $this->assertTrue(Phrase::where([
                ['phrase', '=', $specialPhrase],
                ['language_code', '=', $srcLang],
            ])->count() === 1);
        $this->assertTrue(Translation::count() > $translationCount, 'The translation from outer source did not got saved!');
        $this->assertTrue(Phrase::count() > $phraseCount, 'The phrases from outer source did not got saved!');
    }

    public function testGetTranslation_phraseNotTranslatable()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phraseUserPivotCount = PhraseUserPivot::count();
        $phraseCount = Phrase::count();
        $translationCount = Translation::count();

        $phrase = 'asdfgasdfg';
        $this->assertTrue(Phrase::where([
                ['phrase', '=', $phrase],
                ['language_code', '=', $srcLang],
            ])->count() === 0);

        $translation = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(200);
        $phrases = json_decode($response->getContent(), true)['data'];
        $this->assertTrue(count($phrases) === 0);
        $this->assertTrue($phraseUserPivotCount === PhraseUserPivot::count());
        $this->assertTrue(Phrase::where([
                ['phrase', '=', $phrase],
                ['language_code', '=', $srcLang],
            ])->count() === 0);
        $this->assertTrue(Translation::count() === $translationCount, 'The translation count has been changed!');
        $this->assertTrue(Phrase::count() === $phraseCount, 'The phrases count has been changed!');
    }

    public function testGetTranslation_invalidSrcLanguage_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translation = [
            'src_lang' => 'asdfasdf',
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(412);
    }

    public function testGetTranslation_invalidDstLanguage_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translation = [
            'src_lang' => $srcLang,
            'dst_lang' => 'asdfasdf',
            'phrase' => $phrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(412);
    }

    public function testGetTranslation_missingSrcLanguage_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translation = [
            'src_lang' => '',
            'dst_lang' => $dstLang,
            'phrase' => $phrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(412);
    }

    public function testGetTranslation_missingDstLanguage_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translation = [
            'src_lang' => $srcLang,
            'dst_lang' => '',
            'phrase' => $phrase,
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(412);
    }

    public function testGetTranslation_emptyPhrase_shouldFail()
    {
        //Arrange
        $srcLang = LanguageEnum::hu()->getConstName();
        $dstLang = LanguageEnum::en()->getConstName();
        $phrase = ExternalTranslator::SPECIAL_TEST_PHRASE;
        $hungarianPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $srcLang,
        ]);
        $englishPhrase = Phrase::updateOrCreate([
            'phrase' => $phrase,
            'language_code' => $dstLang,
        ]);
        Translation::updateOrCreate([
            'phrase1_id' => $hungarianPhrase->id,
            'phrase2_id' => $englishPhrase->id,
        ]);
        $translation = [
            'src_lang' => $srcLang,
            'dst_lang' => $dstLang,
            'phrase' => '',
        ];

        //Act
        $response = $this->post('/api/translate', $translation);

        //Assert
        $response->assertStatus(412);
    }
}
