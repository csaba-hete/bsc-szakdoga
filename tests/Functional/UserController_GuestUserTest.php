<?php

namespace Tests\Functional;

use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserController_GuestUserTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testShow_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get('/api/user');

        //Assert
        $response->assertStatus(401);
    }

    public function testGetProfileInfo_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->get('/api/user/details');

        //Assert
        $response->assertStatus(401);
    }

    public function testUpdate_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->put('/api/user', [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'secret',
        ]);

        //Assert
        $response->assertStatus(401);
    }

    public function testDelete_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->delete('/api/user');

        //Assert
        $response->assertStatus(401);
    }

    public function testChangePassword_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post('api/user/change-password');

        //Assert
        $response->assertStatus(401);
    }

    public function testChangeEmail_unauthorized_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post('api/user/change-email');

        //Assert
        $response->assertStatus(401);
    }
}
