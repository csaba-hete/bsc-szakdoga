<?php

namespace Tests\Functional;

use App\Mail\EmailChangeConfirmation;
use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Testing\Fakes\MailFake;
use JWTAuth;
use Tests\TestWithLoggedInUser;

class UserController_RegisteredUserTest extends TestWithLoggedInUser
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testShow()
    {
        //Arrange
        $user = $this->getUser();

        //Act
        $response = $this->get('/api/user', $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => $user->first_name]);
        $response->assertJsonFragment(['last_name' => $user->last_name]);
        $response->assertJsonFragment(['email' => $user->email]);
        $response->assertJsonFragment(['username' => $user->username]);
        $response->assertDontSee('password');
    }

    public function testGetProfileInfo()
    {
        //Arrange
        $user = $this->getUser();

        //Act
        $response = $this->get('/api/user/details', $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => $user->first_name]);
        $response->assertJsonFragment(['last_name' => $user->last_name]);
        $response->assertJsonFragment(['email' => $user->email]);
        $response->assertJsonFragment(['username' => $user->username]);
        $response->assertSee('tests_taken');
        $response->assertSee('avg_test_result');
        $response->assertSee('phrases_searched');
        $response->assertSee('phrases_learned');
        $response->assertSee('avg_phrase_untrained_index');
        $response->assertDontSee('password');
    }

    public function testUpdate()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'first_name' => 'updated.' . $user->first_name,
            'last_name' => 'updated.' . $user->last_name,
            'email' => 'updated.' . $user->email,
            'username' => 'updated.' . $user->username,
        ];

        //Act
        $response = $this->put('/api/user', $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => 'updated.' . $user->first_name]);
        $response->assertJsonFragment(['last_name' => 'updated.' . $user->last_name]);
        $response->assertJsonFragment(['email' => $user->email]);
        $response->assertJsonFragment(['username' => $user->username]);
        $response->assertSee('tests_taken');
        $response->assertSee('avg_test_result');
        $response->assertSee('phrases_searched');
        $response->assertSee('phrases_learned');
        $response->assertSee('avg_phrase_untrained_index');
        $response->assertDontSee('password');
    }

    public function testUpdate_invalidFirstName_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'first_name' => '',
            'last_name' => 'updated.' . $user->last_name,
            'email' => 'updated.' . $user->email,
            'username' => 'updated.' . $user->username,
        ];

        //Act
        $response = $this->put('/api/user', $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testUpdate_invalidLastName_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $updatedUserData = [
            'first_name' => 'updated.' . $user->first_name,
            'last_name' => '',
            'email' => 'updated.' . $user->email,
            'username' => 'updated.' . $user->username,
        ];

        //Act
        $response = $this->put('/api/user', $updatedUserData, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testDelete()
    {
        //Arrange
        $user = $this->getUser();
        $this->assertDatabaseHas('users', [
            'email' => $user->email
        ]);

        //Act
        $response = $this->delete('/api/user', [], $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $this->assertDatabaseMissing('users', [
            'email' => $user->email
        ]);
    }

    public function testChangePassword()
    {
        //Arrange
        $user = $this->getUser();
        $passwordChangeDto = [
            'old_password' => 'mypass',
            'password' => 'mynewpass',
        ];

        //Act
        $response = $this->post('api/user/change-password', $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);
        $this->assertFalse(!!JWTAuth::attempt([
            'email' => $user->email,
            'password' => 'mypass',
        ]));
        $this->assertTrue(!!JWTAuth::attempt([
            'email' => $user->email,
            'password' => 'mynewpass',
        ]));
    }

    public function testChangePassword_invalidOldPassword_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $passwordChangeDto = [
            'old_password' => 'asd',
            'password' => 'mynewpass',
        ];

        //Act
        $response = $this->post('api/user/change-password', $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_incorrectOldPassword_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $passwordChangeDto = [
            'old_password' => 'mypass1',
            'password' => 'mynewpass',
        ];

        //Act
        $response = $this->post('api/user/change-password', $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_missingOldPassword_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $passwordChangeDto = [
            'password' => 'mynewpass',
        ];

        //Act
        $response = $this->post('api/user/change-password', $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_invalidNewPassword_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $passwordChangeDto = [
            'old_password' => 'mypass',
            'password' => 'asd',
        ];

        //Act
        $response = $this->post('api/user/change-password', $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangePassword_missingNewPassword_shouldFail()
    {
        //Arrange
        $user = $this->getUser();
        $passwordChangeDto = [
            'old_password' => 'mypass',
        ];

        //Act
        $response = $this->post('api/user/change-password', $passwordChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangeEmail()
    {
        //Arrange
        $mailer = new MailFake();
        $this->app->instance('mailer', $mailer);

        $user = $this->getUser();
        $oldEmail = $user->email;
        $newEmail = 'john.doe.new@test.com';
        $emailChangeDto = [
            'email' => $newEmail,
        ];
        $url = '';

        //Act
        $response = $this->post('api/user/change-email', $emailChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(200);

        $mailer->assertSent(EmailChangeConfirmation::class, function (EmailChangeConfirmation $mail) use ($oldEmail, &$url) {
            $url = $mail->url;
            return $mail->hasTo($oldEmail);
        });
        $this->get($url);
        $this->assertFalse(!!JWTAuth::attempt([
            'email' => $oldEmail,
            'password' => 'mypass',
        ]));
        $this->assertTrue(!!JWTAuth::attempt([
            'email' => $newEmail,
            'password' => 'mypass',
        ]));
    }

    public function testChangeEmail_invalidEmailAddress()
    {
        //Arrange
        $newEmail = 'john.doe.new@test';
        $emailChangeDto = [
            'email' => $newEmail,
        ];

        //Act
        $response = $this->post('api/user/change-email', $emailChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }

    public function testChangeEmail_alreadyTakenEmailAddress()
    {
        //Arrange
        $newEmail = 'this.email.is.taken@test.com';
        User::create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => $newEmail,
            'username' => 'testuser',
            'password' => 'secret',
        ]);

        $emailChangeDto = [
            'email' => $newEmail,
        ];

        //Act
        $response = $this->post('api/user/change-email', $emailChangeDto, $this->getAuthorizationHeader());

        //Assert
        $response->assertStatus(412);
    }
}
