<?php

namespace Tests\Functional;

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testLogin()
    {
        //Arrange
        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'password',
        ]);

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@slt.com',
            'password' => 'password',
        ]);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@slt.com']);

        $response->assertStatus(200);
        $response->assertJsonFragment(["token_type" => "bearer"]);
        $response->assertSee("access_token");
    }

    public function testLogin_missingUser_shouldFail()
    {
        //Arrange
        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'password',
        ]);

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'nonexistent@slt.com',
            'password' => 'password',
        ]);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@slt.com']);

        $response->assertStatus(404);
    }

    public function testLogin_wrongPassword_shouldFail()
    {
        //Arrange
        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'password',
        ]);

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@slt.com',
            'password' => '123456',
        ]);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@slt.com']);

        $response->assertStatus(401);
    }

    public function testLogin_notValidEmailAddress_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@slt',
            'password' => 'password',
        ]);

        //Assert
        $response->assertStatus(412);
    }

    public function testLogin_shortPassword_shouldFail()
    {
        //Arrange

        //Act
        $response = $this->post('/api/auth/login', [
            'email' => 'test@slt.com',
            'password' => '123',
        ]);

        //Assert
        $response->assertStatus(412);
    }
}
