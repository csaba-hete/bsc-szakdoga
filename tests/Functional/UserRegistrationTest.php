<?php

namespace Tests\Functional;

use App\User;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserRegistrationTest extends TestCase
{
    use DatabaseTransactions;
    use InteractsWithDatabase;

    public function testRegisterUser()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $response->assertStatus(200);
        $response->assertJsonFragment(['first_name' => 'FirstName']);
        $response->assertJsonFragment(['last_name' => 'LastName']);
        $response->assertJsonFragment(['email' => 'test@slt.com']);
        $response->assertJsonFragment(['username' => 'testuser']);
        $response->assertDontSee('password');

        $this->assertDatabaseHas('users', ['email' => 'test@slt.com']);
    }

    public function testRegisterUser_invalidFirstName_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'F',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidLastName_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'L',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidEmail_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt',
            'username' => 'testuser',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidUsername_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt',
            'username' => 'tu',
            'password' => 'secret',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_invalidPassword_shouldFail()
    {
        //Arrange
        $user = [
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => '1234',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $response->assertStatus(412);
    }

    public function testRegisterUser_duplicatedEmail_shouldFail()
    {
        //Arrange

        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test@slt.com',
            'username' => 'testuser',
            'password' => 'password',
        ]);

        $user = [
            'first_name' => 'Test',
            'last_name' => 'Ede',
            'email' => 'test@slt.com',
            'password' => 'password',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test@slt.com']);
        $response->assertStatus(412);
    }

    public function testRegisterUser_duplicatedUsername_shouldFail()
    {
        //Arrange

        factory(User::class)->create([
            'first_name' => 'FirstName',
            'last_name' => 'LastName',
            'email' => 'test1@slt.com',
            'username' => 'testuser',
            'password' => 'password',
        ]);

        $user = [
            'first_name' => 'Test',
            'last_name' => 'Ede',
            'email' => 'test2@slt.com',
            'username' => 'testuser',
            'password' => 'password',
        ];

        //Act
        $response = $this->post('/api/user', $user);

        //Assert
        $this->assertDatabaseHas('users', ['email' => 'test1@slt.com']);
        $response->assertStatus(412);
    }
}
