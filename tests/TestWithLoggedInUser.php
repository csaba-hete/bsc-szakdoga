<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;

class TestWithLoggedInUser extends TestCase
{
    use DatabaseTransactions;

    /** @var  string */
    protected $token;
    /** @var  User */
    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->withoutEvents();
        $this->user = factory(User::class)->make([
            'first_name' => 'John',
            'last_name' => 'Doe',
            'username' => 'john.doe',
            'email' => 'john.doe@test.com',
            'password' => 'mypass'
        ]);
        $this->user->save();

        $this->token = JWTAuth::attempt([
            'email' => 'john.doe@test.com',
            'password' => 'mypass'
        ]);
    }

    /** @return User */
    protected function getUser(): User
    {
        return $this->user;
    }

    /** @return string */
    public function getToken(): string
    {
        return $this->token;
    }

    /** @return array */
    public function getAuthorizationHeader(): array
    {
        return ['HTTP_AUTHORIZATION' => 'Bearer ' . $this->getToken()];
    }
}